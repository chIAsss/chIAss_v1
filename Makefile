SHELL = /bin/sh

#.SUFFIXES:
.SUFFIXES: .cpp .o


CXX=g++
INCLUDE=$(addprefix -I,include include/cute include/chIAss_v1)
FLAGS=-Wall 
LDFLAGS=
LDLIBS=
CHIASS_O_FILES=$(addsuffix .o,$(addprefix chIAss_v1/, CheckCheck ValidKingMove AllPossibleMovesCheck \
IsPinnedDown AllPossibleMoves BoardValue chIAssWaitForOpponent ParseOpening chIAssMove chIAss_v1) )
O_FILES=$(addprefix include/,chess.o $(CHIASS_O_FILES) )
SRC=src/

# CheckCheck could be more efficient by providng a list of pieces?
# modify Play -> give a pointer to a move, modifies the move if needed returns soly a Piece 

exe: src/main.cpp $(O_FILES)
	$(CXX) $(FLAGS) $(INCLUDE) $< $(O_FILES) -o $@

test_chIAss: test/test_chIAss.cpp $(O_FILES)
	$(CXX) $(FLAGS) $(INCLUDE) $< $(O_FILES) -o $@

chiass_v1_test: test/chiass_v1_test.cpp $(O_FILES)
	$(CXX) $(FLAGS) $(INCLUDE) $< $(O_FILES) -o $@

test_mats: test/test_mats.cpp $(O_FILES)
	$(CXX) $(FLAGS) $(INCLUDE) $< $(O_FILES) -o $@

test_time: test/test_time.cpp $(O_FILES)
	$(CXX) $(FLAGS) $(INCLUDE) $< $(O_FILES) -o $@

test_input: test/test_input.cpp $(O_FILES)
	$(CXX) $(FLAGS) $(INCLUDE) $< $(O_FILES) -o $@

include/%.o: src/%.cc include/%.hh
	$(CXX) $(FLAGS) $(INCLUDE) -c $< -o $@

include/chess.o: src/chess.cc include/chess.hh
	$(CXX) $(FLAGS) $(INCLUDE) -c $< -o $@

include/chIAss_v1/AllPossibleMoves.o: src/chIAss_v1/AllPossibleMoves.cc include/chIAss_v1/chiass_v1.hh include/chess.o
	$(CXX) $(FLAGS) $(INCLUDE) -c $< -o $@

include/chIAss_v1/CheckCheck.o: src/chIAss_v1/CheckCheck.cc include/chIAss_v1/chiass_v1.hh include/chess.o
	$(CXX) $(FLAGS) $(INCLUDE) -c $< -o $@

include/chIAss_v1/ValidKingMove.o: src/chIAss_v1/ValidKingMove.cc include/chIAss_v1/chiass_v1.hh include/chess.o
	$(CXX) $(FLAGS) $(INCLUDE) -c $< -o $@

include/chIAss_v1/IsPinnedDown.o: src/chIAss_v1/IsPinnedDown.cc include/chIAss_v1/chiass_v1.hh include/chess.o
	$(CXX) $(FLAGS) $(INCLUDE) -c $< -o $@

include/chIAss_v1/AllPossibleMovesCheck.o: src/chIAss_v1/AllPossibleMovesCheck.cc include/chIAss_v1/chiass_v1.hh include/chess.o
	$(CXX) $(FLAGS) $(INCLUDE) -c $< -o $@

include/chIAss_v1/BoardValue.o: src/chIAss_v1/BoardValue.cc include/chIAss_v1/chiass_v1.hh include/chess.o
	$(CXX) $(FLAGS) $(INCLUDE) -c $< -o $@

include/chIAss_v1/chIAssWaitForOpponent.o: src/chIAss_v1/chIAssWaitForOpponent.cc include/chIAss_v1/chiass_v1.hh include/chess.o
	$(CXX) $(FLAGS) $(INCLUDE) -c $< -o $@

include/chIAss_v1/ParseOpening.o: src/chIAss_v1/ParseOpening.cc include/chIAss_v1/chiass_v1.hh include/chess.o
	$(CXX) $(FLAGS) $(INCLUDE) -c $< -o $@

include/chIAss_v1/chIAssMove.o: src/chIAss_v1/chIAssMove.cc include/chIAss_v1/chiass_v1.hh include/chess.o
	$(CXX) $(FLAGS) $(INCLUDE) -c $< -o $@

include/chIAss_v1/chIAss_v1.o: src/chIAss_v1/chIAss_v1.cc include/chIAss_v1/chiass_v1.hh include/chess.o
	$(CXX) $(FLAGS) $(INCLUDE) -c $< -o $@


clean:
	rm -f chiass_v1_test test_chIAss test_input test_time exe include/*.o include/chIAss_v1/*.o
