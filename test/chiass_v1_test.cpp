#include "cute.h"
#include "cute_runner.h"
#include "ide_listener.h"
#include "cute_counting_listener.h"
#include "cute_test.h"
#include "cute_equals.h"
#include "cute_suite.h"

#include "chess.hh"
#include "chiass_v1.hh"



void TestPawnMoves(void) {
    fprintf( stdout , "Testing PAWN moves\n" );
    fprintf( stdout , "Testing WHITE PAWN\n");
        fprintf( stdout , "\tPAWN alone\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {PAWN, WHITE, e2} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {e2 , e3 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tPAWN blocked\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {PAWN, WHITE, e2} , {PAWN, WHITE, e3} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {e3 , e4 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tPAWN taking\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions_white { {PAWN, WHITE, e2} };
            std::vector<PieceAndPosition> initial_positions_black { {PAWN, BLACK, f3} };
            SetPosition( &empty_board , initial_positions_white , initial_positions_black );
            std::vector< Move > expected { {e2 , e3 }, {e2, f3 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );

            initial_positions_black[0].position = d3;
            SetPosition( &empty_board , initial_positions_white , initial_positions_black );
            expected = { {e2 , e3 }, {e2, d3 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );
            fprintf( stdout , "passed!\n");
        }
// TODO other edge
        fprintf( stdout , "\tPAWN on edge of the map\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions_white { {PAWN, WHITE, a2} };
            std::vector<PieceAndPosition> initial_positions_black { {PAWN, BLACK, b3} , {PAWN, BLACK, h3} };
            SetPosition( &empty_board , initial_positions_white , initial_positions_black );
            std::vector< Move > expected { {a2 , a3 }, {a2, b3 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tPAWN blocked, edged, taking\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions_white { {PAWN, WHITE, a2} };
            std::vector<PieceAndPosition> initial_positions_black { {PAWN, BLACK, a3} , {PAWN, BLACK, b3}, {PAWN, BLACK, h3} };
            SetPosition( &empty_board , initial_positions_white , initial_positions_black );
            std::vector< Move > expected { {a2, b3 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );
            fprintf( stdout , "passed!\n");
        }
    fprintf( stdout , "Testing BLACK PAWN\n");
        fprintf( stdout , "\tPAWN alone\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {PAWN, BLACK, e6} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {e6 , e5 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tPAWN blocked\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {PAWN, BLACK, e6} , {PAWN, BLACK, e5} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {e5 , e4 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tPAWN taking\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions_white { {PAWN, WHITE, e2} };
            std::vector<PieceAndPosition> initial_positions_black { {PAWN, BLACK, f3} };
            SetPosition( &empty_board , initial_positions_white , initial_positions_black );
            std::vector< Move > expected { {f3 , f2 }, {f3, e2 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_black ) );

            initial_positions_white[0].position = g2;
            SetPosition( &empty_board , initial_positions_white , initial_positions_black );
            expected = { {f3 , f2 }, {f3, g2 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_black ) );
            fprintf( stdout , "passed!\n");
        }
// TODO other edge
        fprintf( stdout , "\tPAWN on edge\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions_white { {PAWN, WHITE, b2} , {PAWN, WHITE, h2} };
            std::vector<PieceAndPosition> initial_positions_black { {PAWN, BLACK, a3} };
            SetPosition( &empty_board , initial_positions_white , initial_positions_black );
            std::vector< Move > expected { {a3 , a2 }, {a3, b2 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_black ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tPAWN blocked, edged, taking\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions_white { {PAWN, WHITE, a2}, {PAWN, WHITE, b2}, {PAWN, WHITE, h2}  };
            std::vector<PieceAndPosition> initial_positions_black { {PAWN, BLACK, a3} };
            SetPosition( &empty_board , initial_positions_white , initial_positions_black );
            std::vector< Move > expected { {a3, b2 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_black ) );
            fprintf( stdout , "passed!\n");
        }
}


void TestKnightMoves(void) {
    fprintf( stdout , "Testing KNIGHT moves\n" );
        fprintf( stdout , "\tKNIGHT alone\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {KNIGHT, WHITE, e4} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {e4 , c5 }, {e4 , d6 }, {e4 , f6 }, {e4 , g5 }, {e4 , g3 }, {e4 , f2 }, {e4 , d2 }, {e4 , c3 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }

        fprintf( stdout , "\tKNIGHT blocked\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions_white { {KNIGHT, WHITE, e4} , {PAWN, WHITE, c5}, {PAWN, WHITE, d6}, {PAWN, WHITE, f6}, {PAWN, WHITE, g5}  };
            SetPosition( &empty_board , initial_positions_white );
            std::vector< Move > expected { {e4 , g3 }, {e4 , f2 }, {e4 , d2 }, {e4 , c3 },
            {c5, c6} , {d6, d7}, {f6, f7}, {g5, g6}};// pawn moves
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );

            initial_positions_white = { {KNIGHT, WHITE, e4} , {PAWN, WHITE, g3}, {PAWN, WHITE, f2}, {PAWN, WHITE, d2}, {PAWN, WHITE, c3}  };
            SetPosition( &empty_board , initial_positions_white );
            expected = { {e4 , c5 }, {e4 , d6 }, {e4 , f6 }, {e4 , g5 }, 
             {g3, g4}, {f2, f3}, {d2, d3}, {c3, c4}} ;// pawn moves
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );
            fprintf( stdout , "passed!\n");
        }

        fprintf( stdout , "\tKNIGHT taking\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions_white { {KNIGHT, WHITE, e4} };
            std::vector<PieceAndPosition> initial_positions_black {  {PAWN, BLACK, c5}, {PAWN, BLACK, d6}, {PAWN, BLACK, f6}, {PAWN, BLACK, g5},
            {PAWN, BLACK, c5}, {PAWN, BLACK, d6}, {PAWN, BLACK, f6}, {PAWN, BLACK, g5} };
            SetPosition( &empty_board , initial_positions_white, initial_positions_black );
            std::vector< Move > expected { {e4 , c5 }, {e4 , d6 }, {e4 , f6 }, {e4 , g5 }, {e4 , g3 }, {e4 , f2 }, {e4 , d2 }, {e4 , c3 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );
            fprintf( stdout , "passed!\n");
        }

        fprintf( stdout , "\tKNIGHT on the edge\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions_white { {KNIGHT, WHITE, e2} };
            SetPosition( &empty_board , initial_positions_white );
            std::vector< Move > expected { {e2 , c3}, {e2 , d4}, {e2 , f4}, {e2 , g3}, {e2 , g1}, {e2 , c1} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );

            initial_positions_white[0].position = e1;
            SetPosition( &empty_board , initial_positions_white );
            expected = { {e1 , c2}, {e1 , d3}, {e1 , f3}, {e1 , g2} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );

            initial_positions_white[0].position = e7;
            SetPosition( &empty_board , initial_positions_white );
            expected = { {e7,c8}, {e7,g8}, {e7 , g6}, {e7 , f5}, {e7 , d5}, {e7 , c6} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );

            initial_positions_white[0].position = a4;
            SetPosition( &empty_board , initial_positions_white );
            expected = { {a4 , b6}, {a4 , c5}, {a4 , c3}, {a4 , b2} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );

            initial_positions_white[0].position = h4;
            SetPosition( &empty_board , initial_positions_white );
            expected = { {h4 , f5}, {h4 , g6}, {h4 , g2}, {h4 , f3} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );

            fprintf( stdout , "passed!\n");
        }
// TODO full ?
}

void TestBishopMoves(void) {
    fprintf( stdout , "Testing BISHOP moves\n" );
        fprintf( stdout , "\tBISHOP alone\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {BISHOP, WHITE, e4} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {e4 , d5 }, {e4 , c6 }, {e4 , b7}, {e4, a8}, 
            {e4 , f5 }, {e4 , g6 }, {e4 , h7 }, 
            {e4 , f3 }, {e4 , g2 }, {e4 , h1 },
            {e4 , d3 }, {e4 , c2 }, {e4 , b1 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tBISHOP blocked\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {BISHOP, WHITE, e4}, {PAWN, WHITE, c6}, {PAWN, WHITE, g6}, {PAWN, WHITE, f3}, {PAWN, WHITE, c2}  };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {e4 , d5},  
            {e4 , f5}, 
            {e4 , d3},
            {c6, c7}, {g6, g7}, {f3, f4}, {c2, c3} } ;
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tBISHOP taking\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions_white { {BISHOP, WHITE, e4}};
            std::vector<PieceAndPosition> initial_positions_black{ {PAWN, BLACK, b7}, {PAWN, BLACK, g6}, {PAWN, BLACK, f3}, {PAWN, BLACK, c2}  };
            SetPosition( &empty_board , initial_positions_white, initial_positions_black );
            std::vector< Move > expected { {e4, d5}, {e4, c6}, {e4, b7},
            {e4, f5}, {e4, g6},
            {e4, f3},
            {e4, d3}, {e4, c2} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tBISHOP on the edge\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {BISHOP, WHITE, a4} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {a4 , b5 }, {a4 , c6 }, {a4 , d7}, {a4, e8}, 
            {a4 , b3 }, {a4 , c2 }, {a4 , d1 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
// TODO all ?

}

void TestRookMoves(void) {
    fprintf( stdout , "Testing ROOK moves\n" );
        fprintf( stdout , "\tROOK alone\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {ROOK, WHITE, e4} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {e4 , e5 }, {e4 , e6}, {e4 , e7}, {e4, e8}, 
            {e4 , e3}, {e4 , e2}, {e4 , e1}, 
            {e4 , f4}, {e4 , g4}, {e4 , h4},
            {e4 , d4}, {e4 , c4}, {e4 , b4}, {e4, a4} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }

        fprintf( stdout , "\tROOK blocked\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {ROOK, WHITE, e4} , {PAWN, WHITE, e6}, {PAWN, WHITE, e1}, {PAWN, WHITE, g4}, {PAWN, WHITE, d4} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {e4 , e5 }, 
            {e4 , e3}, {e4 , e2}, 
            {e4 , f4},
            {e6, e7}, {e1, e2}, {g4, g5}, {d4, d5} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }

        fprintf( stdout , "\tROOK taking\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions_white { {ROOK, WHITE, e4} };
            std::vector<PieceAndPosition> initial_positions_black { {PAWN, BLACK, e6}, {PAWN, BLACK, e1}, {PAWN, BLACK, g4}, {PAWN, BLACK, d4} };
            SetPosition( &empty_board , initial_positions_white, initial_positions_black );
            std::vector< Move > expected { {e4 , e5 }, {e4 , e6},
            {e4 , e3}, {e4 , e2}, {e4 , e1}, 
            {e4 , f4}, {e4 , g4},
            {e4 , d4}};
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tROOK on the edge\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {ROOK, WHITE, a4} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {a4 , a5 }, {a4 , a6}, {a4 , a7}, {a4, a8}, 
            {a4, a3}, {a4, a2}, {a4, a1}, 
            {a4, b4}, {a4, c4}, {a4, d4}, {a4, e4}, {a4, f4}, {a4, g4}, {a4, h4} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
// TODO all ?
}


void TestQueenMoves(void) {
    fprintf( stdout , "Testing QUEEN moves\n" );
        fprintf( stdout , "\tQUEEN alone\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {QUEEN, WHITE, e4} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {e4 , d5 }, {e4 , c6 }, {e4 , b7}, {e4, a8}, 
            {e4 , f5 }, {e4 , g6 }, {e4 , h7 }, 
            {e4 , f3 }, {e4 , g2 }, {e4 , h1 },
            {e4 , d3 }, {e4 , c2 }, {e4 , b1 },
            {e4 , e5 }, {e4 , e6}, {e4 , e7}, {e4, e8}, 
            {e4 , e3}, {e4 , e2}, {e4 , e1}, 
            {e4 , f4}, {e4 , g4}, {e4 , h4},
            {e4 , d4}, {e4 , c4}, {e4 , b4}, {e4, a4} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tQUEEN blocked\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {QUEEN, WHITE, e4}, {PAWN, WHITE, d5}, {PAWN, WHITE, g6}, {PAWN, WHITE, f3}, {PAWN, WHITE, c2}, {PAWN, WHITE, e6}, {PAWN, WHITE, e1}, {PAWN, WHITE, g4}, {PAWN, WHITE, d4} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {e4 , f5}, 
            {e4 , d3},
            {e4 , e5 }, 
            {e4 , e3}, {e4 , e2}, 
            {e4 , f4},
            {d5, d6}, {g6, g7}, {f3, f4}, {c2, c3},
            {e6, e7}, {e1, e2}, {g4, g5} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tQUEEN taking\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions_white { {QUEEN, WHITE, e4}};
            std::vector<PieceAndPosition> initial_positions_black{ {PAWN, BLACK, b7}, {PAWN, BLACK, g6}, {PAWN, BLACK, f3}, {PAWN, BLACK, c2}, {PAWN, BLACK, e6}, {PAWN, BLACK, e1}, {PAWN, BLACK, g4}, {PAWN, BLACK, d4} };
            SetPosition( &empty_board , initial_positions_white, initial_positions_black );
            std::vector< Move > expected { {e4, d5}, {e4, c6}, {e4, b7},
            {e4, f5}, {e4, g6},
            {e4, f3},
            {e4, d3}, {e4, c2},
            {e4 , e5 }, {e4 , e6},
            {e4 , e3}, {e4 , e2}, {e4 , e1}, 
            {e4 , f4}, {e4 , g4},
            {e4 , d4} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tQUEEN on the edge\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {QUEEN, WHITE, a4} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {a4 , b5 }, {a4 , c6 }, {a4 , d7}, {a4, e8}, 
            {a4 , b3 }, {a4 , c2 }, {a4 , d1 },
            {a4 , a5 }, {a4 , a6}, {a4 , a7}, {a4, a8}, 
            {a4, a3}, {a4, a2}, {a4, a1}, 
            {a4, b4}, {a4, c4}, {a4, d4}, {a4, e4}, {a4, f4}, {a4, g4}, {a4, h4} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
}

void TestKingMoves(void) {
    fprintf( stdout , "Testing KING moves\n" );
        fprintf( stdout , "\tKING alone\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {KING, WHITE, e4} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {e4 , d4}, 
            {e4 , d5 }, 
            {e4 , e5 },
            {e4 , e3},
            {e4 , d3 },
            {e4 , f3 },
            {e4 , f4},
            {e4 , f5 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tKING blocked\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {KING, WHITE, e4}, {PAWN, WHITE, d4}, {PAWN, WHITE, d5}, {PAWN, WHITE, e5}, {PAWN, WHITE, e3}, {PAWN, WHITE, d3}, {PAWN, WHITE, f3}, {PAWN, WHITE, f4}, {PAWN, WHITE, f5} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {d5, d6},
            {e5, e6}, 
            {f5, f6} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tKING taking\t\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions_white { {KING, WHITE, e4} };
            std::vector<PieceAndPosition> initial_positions_black { {PAWN, BLACK, d3}, {PAWN, BLACK, d4}, {PAWN, BLACK, d5}, {PAWN, BLACK, e5}, {PAWN, BLACK, e3}, {PAWN, BLACK, f3}, {PAWN, BLACK, f4}, {PAWN, BLACK, f5} };
            SetPosition( &empty_board , initial_positions_white, initial_positions_black );
            std::vector< Move > expected { {e4, d5},
            {e4 , e5 },
            {e4 , d3 },
            {e4 , f3 },
            {e4 , f5 }};
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );
            fprintf( stdout , "passed!\n");
        }
        fprintf( stdout , "\tKING on the edge\t\t" ); {
            Board empty_board;
            std::vector<PieceAndPosition> initial_positions { {KING, WHITE, a4} };
            SetPosition( &empty_board , initial_positions );
            std::vector< Move > expected { {a4, b4},
            {a4, a5},
            {a4, b5}, 
            {a4, a3},
            {a4, b3} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );

            initial_positions = { {KING, WHITE, e1} };
            SetPosition( &empty_board , initial_positions );
            expected = { {e1, d1}, {e1, d2}, {e1, e2}, {e1, f1}, {e1, f2} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );

            initial_positions = { {KING, WHITE, h4} };
            SetPosition( &empty_board , initial_positions );
            expected = { {h4, g4}, {h4, g5}, {h4, h5}, {h4, h3}, {h4, g3} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );

            initial_positions = { {KING, WHITE, e8} };
            SetPosition( &empty_board , initial_positions );
            expected = { {e8, d8}, {e8, e7}, {e8, d7}, {e8, f7}, {e8, f8} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions ) );
            fprintf( stdout , "passed!\n");
        }
}

void TestMiscellaneousMoves(void) {
    fprintf( stdout , "Testing pinned pieces\t\t\t" ); {
        Board empty_board;
        std::vector<PieceAndPosition> initial_positions_white { {KING, WHITE, e1}, {PAWN, WHITE, e2} };
        std::vector<PieceAndPosition> initial_positions_black { {ROOK, BLACK, e8}, {PAWN, BLACK, f3 } };
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        std::vector< Move > expected { {e1, d1}, 
        {e1, d2}, 
        {e1, f1},
        {e1, f2},
        {e2, e3} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );
        
        initial_positions_white = { {KING, WHITE, a1}, {BISHOP, WHITE, c3} };
        initial_positions_black = { {BISHOP, BLACK, h8} };
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        expected = { {a1, b1}, 
        {a1, a2},
        {a1, b2}, 
        {c3, d4},
        {c3, e5},
        {c3, f6},
        {c3, g7},
        {c3, h8},
        {c3, b2} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );
        
        initial_positions_white = { {KING, WHITE, a4}, {QUEEN, WHITE, c4} };
        initial_positions_black = { {ROOK, BLACK, h4} };
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        expected = {{a4, b4},
        {a4, a5},
        {a4, b5}, 
        {a4, a3},
        {a4, b3}, 
        {c4, d4}, 
        {c4, e4}, 
        {c4, f4}, 
        {c4, g4}, 
        {c4, h4}, 
        {c4, b4} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &empty_board , initial_positions_white ) );
        fprintf( stdout , "passed!\n");  
    }
    fprintf( stdout , "Testing check\n" ); {
        Board empty_board;
        std::vector<PieceAndPosition> initial_positions_white;
        std::vector<PieceAndPosition> initial_positions_black;
        Move last_move;
        initial_positions_white = { {KING, WHITE, e1} };
        initial_positions_black = { {ROOK, BLACK, a2}, {BISHOP, BLACK, d4}, {KNIGHT, BLACK, c7} };
        last_move.beg = a4;
        last_move.end = a2;
        fprintf( stdout , "\tEmpty check\t\t\t");
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 0 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );
        initial_positions_white = { {KING, WHITE, e1} };
        fprintf( stdout , "passed!\n"); 
// ROOK check
        fprintf( stdout , "\tROOK check\t\t\t");
        initial_positions_white = { {KING, WHITE, e1} };
        initial_positions_black = { {ROOK, BLACK, h1},{ROOK, BLACK, b2}, {ROOK, BLACK, c3}, {ROOK, BLACK, d4} };
        last_move.beg = h4;
        last_move.end = h1;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 40 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        initial_positions_white = { {KING, WHITE, e1} };
        initial_positions_black = { {ROOK, BLACK, b2}, {BISHOP, BLACK, a5}, {ROOK, BLACK, d4} };
        last_move.beg = b4;
        last_move.end = b2;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 5 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        initial_positions_white = { {KING, WHITE, e1} };
        initial_positions_black = { {ROOK, BLACK, e3}, {BISHOP, BLACK, h4}, {ROOK, BLACK, d4} };
        last_move.beg = g3;
        last_move.end = e3;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 17 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        initial_positions_white = { {KING, WHITE, g3}, {PAWN, WHITE, h4}, {PAWN, WHITE, g2}, 
        {PAWN, WHITE, e3}, {PAWN, WHITE, a5}, {BISHOP, WHITE, c3}, {KNIGHT, WHITE, f4}, {ROOK, WHITE, c6} };
        initial_positions_black = { {KING, BLACK, h6}, {PAWN, BLACK, h7}, {PAWN, BLACK, f5},
        {ROOK, BLACK, a2}, {ROOK, BLACK, f1}};
        last_move.beg = c5;
        last_move.end = c6;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 30 , (int) CheckCheck(& empty_board , last_move , initial_positions_black[0] ) );

        initial_positions_white = { {KING, WHITE, g3}, {PAWN, WHITE, h4}, {PAWN, WHITE, g2}, 
        {PAWN, WHITE, e3}, {PAWN, WHITE, a5}, {BISHOP, WHITE, c3}, {KNIGHT, WHITE, f4}, {ROOK, WHITE, (square)37} };
        initial_positions_black = { {KING, BLACK, h6}, {PAWN, BLACK, h7}, {PAWN, BLACK, f5},
        {ROOK, BLACK, a2}, {ROOK, BLACK, f1}};
        last_move.beg = c5;
        last_move.end = (square)37;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 0 , (int) CheckCheck(& empty_board , last_move , initial_positions_black[0] ) );
        fprintf( stdout , "passed!\n"); 
// BISHOP check
        fprintf( stdout , "\tBISHOP check\t\t\t");
        initial_positions_white = { {KING, WHITE, e1} };
        initial_positions_black = { {ROOK, BLACK, b2}, {BISHOP, BLACK, d2} };
        last_move.beg = c1;
        last_move.end = d2;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 50 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        initial_positions_white = { {KING, WHITE, e1} };
        initial_positions_black = { {ROOK, BLACK, a1}, {BISHOP, BLACK, b3} };
        last_move.beg = b1;
        last_move.end = b3;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 3 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        initial_positions_black = { {ROOK, BLACK, e8}, {BISHOP, BLACK, g3} };
        last_move.beg = e5;
        last_move.end = g3;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 71 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );
        fprintf( stdout , "passed!\n"); 
// KNIGHT check
        fprintf( stdout , "\tKNIGHT check\t\t\t");
        initial_positions_white = { {KING, WHITE, e1} };
        initial_positions_black = { {KNIGHT, BLACK, c2}, {BISHOP, BLACK, d8} };
        last_move.beg = b4;
        last_move.end = c2;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 100 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        initial_positions_white = { {KING, WHITE, e8} };
        initial_positions_black = { {KNIGHT, BLACK, d4}, {BISHOP, BLACK, a4} };
        last_move.beg = b5;
        last_move.end = d4;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 8 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        initial_positions_white = { {KING, WHITE, c8} };
        initial_positions_black = { {KNIGHT, BLACK, d6}, {QUEEN, BLACK, h3} };
        last_move.beg = f5;
        last_move.end = d6;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 106 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        initial_positions_white = { {KING, WHITE, c8} };
        initial_positions_black = { {KNIGHT, BLACK, d6}, {QUEEN, BLACK, c1} };
        last_move.beg = c4;
        last_move.end = d6;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 102 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );
        fprintf( stdout , "passed!\n"); 
//PAWN check
        fprintf( stdout , "\tPAWN check\t\t\t");
        initial_positions_white = { {KING, WHITE, c3} };
        initial_positions_black = { {PAWN, BLACK, d4}, {BISHOP, BLACK, d8} };
        last_move.beg = d5;
        last_move.end = d4;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 70 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        initial_positions_white = { {KING, WHITE, c3} };
        initial_positions_black = { {PAWN, BLACK, d5}, {QUEEN, BLACK, c8} };
        last_move.beg = c6;
        last_move.end = d5;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 1 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        initial_positions_white = { {KING, WHITE, c3} };
        initial_positions_black = { {PAWN, BLACK, e4}, {QUEEN, BLACK, g7} };
        last_move.beg = e5;
        last_move.end = e4;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 7 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        initial_positions_white = { {KING, WHITE, c3} };
        initial_positions_black = { {PAWN, BLACK, d4}, {QUEEN, BLACK, c8} };
        last_move.beg = c5;
        last_move.end = d4;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 71 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        fprintf( stdout , "passed!\n"); 
//QUEEN check
        fprintf( stdout , "\tQUEEN check\t\t\t");
        initial_positions_black = { {QUEEN, BLACK, b2}, {BISHOP, BLACK, d8} };
        last_move.beg = b8;
        last_move.end = b2;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 80 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );


        initial_positions_white = { {KING, WHITE, b1}, {PAWN, WHITE, b2}, {PAWN, WHITE, c2}, 
        {PAWN, WHITE, d5}, {PAWN, WHITE, f3}, {PAWN, WHITE, g6}, {BISHOP, WHITE, f5}, {ROOK, WHITE, c1},
        {QUEEN, WHITE, e7} };
        initial_positions_black = { {KING, BLACK, c7}, {PAWN, BLACK, a7}, {PAWN, BLACK, b4},
        {PAWN, BLACK, d6}, {PAWN, BLACK, e5}, {ROOK, BLACK, a2}, {ROOK, BLACK, b8}, {QUEEN, BLACK, a5}};
        last_move = {f8, e7};
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 40 , (int) CheckCheck(& empty_board , last_move , initial_positions_black[0] ) );

        fprintf( stdout , "passed!\n"); 
// Promotion
        fprintf( stdout , "\tPromotion check\t\t\t");
        initial_positions_white = { {KING, WHITE, e1} };
        initial_positions_black = { {QUEEN, BLACK, c1}, {BISHOP, BLACK, d8} };
        last_move.beg = c2;
        last_move.end = c1;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 30 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        initial_positions_black = { {QUEEN, BLACK, d1}, {ROOK, BLACK, e8} };
        last_move.beg = e2;
        last_move.end = d1;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 31 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        initial_positions_white = { {KING, WHITE, e2} };
        initial_positions_black = { {QUEEN, BLACK, f1}, {QUEEN, BLACK, h2} };
        last_move.beg = f2;
        last_move.end = f1;
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( 64 , (int) CheckCheck(& empty_board , last_move , initial_positions_white[0] ) );

        fprintf( stdout , "passed!\n"); 
        fprintf( stdout , "Testing check\t\t\t\tpassed!\n");  
    }

    fprintf( stdout , "Testing checked\n"); {
        Board empty_board;
        std::vector<PieceAndPosition> initial_positions_white;
        std::vector<PieceAndPosition> initial_positions_black;
        Move last_move;
        std::vector< Move > expected;

        last_move.beg = a5;
        last_move.end = a3;
        initial_positions_white = { {KING, WHITE, e3} };
        initial_positions_black = { {ROOK, BLACK, a3}, {KING, BLACK, e5} };
        expected = { {e3, e2}, {e3, d2}, {e3, f2} };
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( expected, AllPossibleMovesCheck(&empty_board, initial_positions_white, last_move, initial_positions_white[0], CheckCheck(&empty_board, last_move, initial_positions_white[0]) ) );


        last_move.beg = a1;
        last_move.end = a8;
        initial_positions_white = { {KING, WHITE, e8}, {ROOK, WHITE, d5} };
        initial_positions_black = { {ROOK, BLACK, a8}, {ROOK, BLACK, b7} };
        expected = { {d5, d8} };
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( expected, AllPossibleMovesCheck(&empty_board, initial_positions_white, last_move, initial_positions_white[0], CheckCheck(&empty_board, last_move, initial_positions_white[0]) ) );


        last_move.beg = b8;
        last_move.end = d7;
        initial_positions_white = { {KING, WHITE, f8}, {ROOK, WHITE, b5} };
        initial_positions_black = { {ROOK, BLACK, a8}, {KNIGHT, BLACK, d7} };
        expected = { {f8, f7}, {f8, e7}, {f8, g7} };
        SetPosition( &empty_board , initial_positions_white, initial_positions_black );
        ASSERT_EQUAL( expected, AllPossibleMovesCheck(&empty_board, initial_positions_white, last_move, initial_positions_white[0], CheckCheck(&empty_board, last_move, initial_positions_white[0]) ) );

        fprintf( stdout , "Testing checked\t\t\t\tpassed!\n"); 
    }
}

void RandomTest(void) {
    fprintf(stdout , "Testing specific case\n"); {
        Board empty_board;
        std::vector<PieceAndPosition> initial_positions_b { {KING, BLACK, a5}, {PAWN, BLACK, b5}, {BISHOP, BLACK, g3},
        {QUEEN, BLACK, a1}, {ROOK, BLACK, e3} };
        std::vector<PieceAndPosition> initial_positions_w { {KING, WHITE, f1}, {PAWN, WHITE, g2}, {PAWN, WHITE, b3},
        {PAWN, WHITE, a2}, {ROOK, WHITE, b7}, {QUEEN, WHITE, h4}};
        SetPosition( &empty_board , initial_positions_w, initial_positions_b );
        Move last_move = {e5, a1};
        ASSERT_EQUAL( 30 , (int) CheckCheck( &empty_board , last_move , initial_positions_w[0] ) );
    }
    fprintf( stdout , "Testing specific case\t\t\tpassed!\n"); 
}

int main(void) {
	cute::ide_listener<> listener{};

    cute::suite suite_of_tests;
    suite_of_tests.push_back( CUTE(TestPawnMoves));
    suite_of_tests.push_back( CUTE(TestKnightMoves));
    suite_of_tests.push_back( CUTE(TestBishopMoves));
    suite_of_tests.push_back( CUTE(TestRookMoves));
    suite_of_tests.push_back( CUTE(TestQueenMoves));
    suite_of_tests.push_back( CUTE(TestKingMoves));
    suite_of_tests.push_back( CUTE(TestMiscellaneousMoves));
    suite_of_tests.push_back( CUTE(RandomTest));
	cute::makeRunner(listener)(suite_of_tests,"All tests\n");

    return 0;
}