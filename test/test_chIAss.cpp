#include "chess.hh"
#include "chiass_v1.hh"
#include <chrono>

void NonMate(void) {
    fprintf( stdout , "Testing non mate\n" );
    fprintf( stdout , "\tBenjamin opening\t\t\t"); {
        Board b;
        InitBoard(&b);
        std::vector<Move> moves = { {e2,e4}, {e7,e5}, {g1,f3}, {b8,c6}, {f1,c4}, {g8,f6}, {f3,g5}, {h7,h6}};
        UpdateBoard( &b , moves );

        std::vector<PieceAndPosition> white_pieces;
        std::vector<PieceAndPosition> black_pieces;

        for ( unsigned char i = 0 ; i < 64 ; i++ ) {
            PieceAndPosition added_piece;
            if ( b.B[i].piece != EMPTY ) {
                added_piece.piece = b.B[i].piece;
                added_piece.colour = b.B[i].colour;
                added_piece.position = (square) i;
                switch ( b.B[i].colour ) {
                    case WHITE:
                        white_pieces.push_back( added_piece );
                        break;
                    case BLACK:
                        black_pieces.push_back( added_piece );
                }
        // the king is in first place of the vector
                if ( b.B[i].piece >= KING ) {
                    PieceAndPosition temporary;
                    switch (  b.B[i].colour ) {
                        case WHITE:
                            temporary = white_pieces[0];
                            white_pieces[0] = white_pieces.back();
                            white_pieces.back() = temporary;
                            break;
                        case BLACK:
                            temporary = black_pieces[0];
                            black_pieces[0] = black_pieces.back();
                            black_pieces.back() = temporary;
                    }
                }
            }
        }
//        DisplayBoard( b );
        chIAssMove( &b , 0, 4, white_pieces, black_pieces, moves.back()).m;
    }
}


int main(void) {

    Board b;
    InitBoard( &b );
    std::string opening;
    chIAss_v1( b, opening, WHITE , 4);
//    NonMate();

}