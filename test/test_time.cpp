#include "cute.h"
#include "cute_runner.h"
#include "ide_listener.h"
#include "cute_test.h"
#include "cute_equals.h"
#include "cute_suite.h"

#include "chess.hh"
#include "chiass_v1.hh"
#include <chrono>

void TestTimeFullBoard(void) {
	fprintf(stdout, "Testing performance on a full board\n");
	Board b;
	InitBoard( &b);
	std::vector<Move> m ={ {d2,d4}, {d7,d5}, {b1,c3}, {g8,g6}, {c1,f4}, {b8,c6}};
	UpdateBoard( &b, m);

    std::vector<PieceAndPosition> white_pieces;
    std::vector<PieceAndPosition> black_pieces;

    for ( unsigned char i = 0 ; i < 64 ; i++ ) {
        PieceAndPosition added_piece;
		if ( b.B[i].piece != EMPTY ) {
			added_piece.piece = b.B[i].piece;
			added_piece.colour = b.B[i].colour;
			added_piece.position = (square) i;
			switch ( b.B[i].colour ) {
				case WHITE:
					white_pieces.push_back( added_piece );
					break;
				case BLACK:
					black_pieces.push_back( added_piece );
			}
	// the king is in first place of the vector
			if ( b.B[i].piece >= KING ) {
				PieceAndPosition temporary;
				switch (  b.B[i].colour ) {
					case WHITE:
						temporary = white_pieces[0];
						white_pieces[0] = white_pieces.back();
						white_pieces.back() = temporary;
						break;
					case BLACK:
						temporary = black_pieces[0];
						black_pieces[0] = black_pieces.back();
						black_pieces.back() = temporary;
				}
			}
		}
    }

	fprintf(stdout,"\n");
	DisplayBoard( b );
	fprintf(stdout,"\n");

	fprintf(stdout, "\tDepth = 2 :\t");
	auto start = std::chrono::steady_clock::now();
	chIAssMove( &b , 0, 2, white_pieces, black_pieces, m.back());
	auto end = std::chrono::steady_clock::now();
    std::cout<< "Time :" << std::chrono::duration_cast<std::chrono::seconds>(end - start).count()<<" s\n";

	fprintf(stdout, "\tDepth = 4 :\t");
	start = std::chrono::steady_clock::now();
	chIAssMove( &b , 0, 4, white_pieces, black_pieces, m.back());
	end = std::chrono::steady_clock::now();
    std::cout<< "Time :" << std::chrono::duration_cast<std::chrono::seconds>(end - start).count()<<" s\n";

	fprintf(stdout, "\tDepth = 5 :\t");
	start = std::chrono::steady_clock::now();
	chIAssMove( &b , 0, 5, white_pieces, black_pieces, m.back());
	end = std::chrono::steady_clock::now();
    std::cout<< "Time :" << std::chrono::duration_cast<std::chrono::seconds>(end - start).count()<<" s\n";

	fprintf(stdout, "\tDepth = 6 :\t");
	start = std::chrono::steady_clock::now();
	chIAssMove( &b , 0, 6, white_pieces, black_pieces, m.back());
	end = std::chrono::steady_clock::now();
    std::cout<< "Time :" << std::chrono::duration_cast<std::chrono::seconds>(end - start).count()<<" s\n";

	fprintf(stdout, "\tDepth = 7\n");
	start = std::chrono::steady_clock::now();
	chIAssMove( &b , 0, 7, white_pieces, black_pieces, m.back());
	end = std::chrono::steady_clock::now();
    std::cout<< "Time :" << std::chrono::duration_cast<std::chrono::seconds>(end - start).count()<<" s\n";

}



void TestTimeMidGamePosition(void) {
	fprintf(stdout, "Testing performance midgame position\n");
	Board b;
    std::vector<PieceAndPosition> white_pieces = { {KING,WHITE,g1}, {PAWN,WHITE,a6}, {PAWN,WHITE,c2}, {PAWN,WHITE,e5},
	{PAWN,WHITE,f4}, {PAWN,WHITE,g2}, {PAWN,WHITE,h2},
	{BISHOP,WHITE,e4}, {BISHOP,WHITE,c1}, {ROOK,WHITE,b3}, {ROOK,WHITE,f1}, {QUEEN,WHITE,d3}  };
    std::vector<PieceAndPosition> black_pieces = { {KING,BLACK,b8}, {PAWN,BLACK,a7}, {PAWN,BLACK,b6}, {PAWN,BLACK,c3},
	{PAWN,BLACK,d4}, {PAWN,BLACK,e6}, {PAWN,BLACK,f7}, 
	{KNIGHT,BLACK,e3}, {KNIGHT,BLACK,f5}, {ROOK,BLACK,d7}, {ROOK,BLACK,g8}, {QUEEN,BLACK,e7} };
	SetPosition( &b , white_pieces, black_pieces);
	Move m ={h6,f5};
	std::cout << m;

	fprintf(stdout,"\n");
	DisplayBoard( b );
	fprintf(stdout,"\n");

	fprintf(stdout, "\tDepth = 2 :\t");
	auto start = std::chrono::steady_clock::now();
	chIAssMove( &b , 0, 2, white_pieces, black_pieces, m);
	auto end = std::chrono::steady_clock::now();
    std::cout<< "Time :" << std::chrono::duration_cast<std::chrono::seconds>(end - start).count()<<" s\n";

	fprintf(stdout, "\tDepth = 4 :\t");
	start = std::chrono::steady_clock::now();
	chIAssMove( &b , 0, 4, white_pieces, black_pieces, m);
	end = std::chrono::steady_clock::now();
    std::cout<< "Time :" << std::chrono::duration_cast<std::chrono::seconds>(end - start).count()<<" s\n";

	fprintf(stdout, "\tDepth = 5 :\t");
	start = std::chrono::steady_clock::now();
	chIAssMove( &b , 0, 5, white_pieces, black_pieces, m);
	end = std::chrono::steady_clock::now();
    std::cout<< "Time :" << std::chrono::duration_cast<std::chrono::seconds>(end - start).count()<<" s\n";

	fprintf(stdout, "\tDepth = 6 :\t");
	start = std::chrono::steady_clock::now();
	chIAssMove( &b , 0, 6, white_pieces, black_pieces, m);
	end = std::chrono::steady_clock::now();
    std::cout<< "Time :" << std::chrono::duration_cast<std::chrono::seconds>(end - start).count()<<" s\n";
}



int main(void) {
	TestTimeMidGamePosition();

}
