#include "cute.h"
#include "cute_runner.h"
#include "ide_listener.h"
#include "cute_test.h"
#include "cute_equals.h"
#include "cute_suite.h"

#include "chess.hh"
#include "chiass_v1.hh"




void TestInput(void) {
    fprintf( stdout , "Testing input\n" );
    Board test_board;
    Move expected;
    Move m;
    InitBoard( &test_board );

    fprintf( stdout , "\tPlease enter e2\ne4\n" );
    m = chIAssWaitForOpponent( );
    Play( &test_board , m );
    expected = {e2, e4};
    DisplayBoard(test_board);
    ASSERT_EQUAL( expected , m );

    fprintf( stdout , "\n\tPlease enter f1\nc4\n" );
    m = chIAssWaitForOpponent( );
    Play( &test_board , m );
    expected = {f1, c4};
    DisplayBoard(test_board);
    ASSERT_EQUAL( expected , m );

    fprintf( stdout , "\n\tPlease enter g1\nf3\n" );
    m = chIAssWaitForOpponent( );
    Play( &test_board , m );
    expected = {g1, f3};
    DisplayBoard(test_board);
    ASSERT_EQUAL( expected , m );

    fprintf( stdout , "\n\tPlease enter crw 2 times, for Castle right White\n" );
    m = chIAssWaitForOpponent( );
    Play( &test_board , m );
    expected = {(square)0, (square)102};
    DisplayBoard(test_board);
    ASSERT_EQUAL( expected , m );
    
    fprintf( stdout , "Testing input\t\t\tpassed!\n" );
}



int main(void) {
    unsigned short number_of_test = 0;
    unsigned short test_passed = 0;
	cute::ide_listener<> listener{};

    number_of_test++;
    if (cute::makeRunner(listener)(TestInput)){
        fprintf( stdout , "Testing input\t\t\tpassed!\n" );
        test_passed++;
    } else {
        fprintf( stdout , "Testing input\t\t\tfailure\n" );
    }



    fprintf(stdout , "\nPassed %d/%d test\n", test_passed, number_of_test );
    return 0;
}