#ifndef CHIASS_V1_HH
#define CHIASS_V1_HH
#include "chess.hh"
#include <string>

typedef struct _tree_moves{
    Move m;
// supposedly enough, unless 9 queens, maybe
    unsigned char n_next;
    struct _tree_moves* next;
} TreeMoves;

typedef struct _valued_move {
    Move m;
    fast32 value;
} ValuedMove;
inline bool operator==(const Move& m1, const ValuedMove& m2) {
	return m1.beg == m2.m.beg && m1.end == m2.m.end;
}
inline std::ostream& operator<<(std::ostream& os, const ValuedMove& m) {
    os << static_cast<char>(m.m.beg%8 +97)  << (int) m.m.beg/8 + 1<< ',' << static_cast<char>(m.m.end%8+97) << (int)m.m.end/8 + 1;
    return os;
}

unsigned char IsPinnedDown( const Board* b , const PieceAndPosition p );

bool ValidKingMove( const Board b , const Move king );
/*
 * \brief Function used to find all possible moves in a non-check situation
 * \param b The board
 * \param list_pieces the pieces of the player
 * \return vector of all possible moves
 */
std::vector<Move> AllPossibleMoves( const Board* b, const std::vector<PieceAndPosition> list_pieces );
std::vector<Move> AllPossibleMovesCheck( const Board* b, const std::vector<PieceAndPosition> list_pieces, Move last_move_played, PieceAndPosition king, u8 result );
// Notes : The /8 operation for a "greater than" compartison is never necessary, it's only used for eaulity
// TODO : fuck the pawn case
/*
 * \brief Determines if the player is in check
 * \brief the move HAS TO have been played on the board. Otherwise, the comportment can be unexpected.
 * \brief ( if the last move was a promotion, the piece is therefore considered as promoted )
 * \param b The board
 * \param last_move_played the last move played
 * \param king the position of the king
 * \return 0 if the player is not in check \
 * \return for a pawn check ( take pawn or king moves )
 * \return for a double check ( only the king can move ) 
 * \return for a knight check ( take knight or king moves )
 * \return 1 if the player is in check from a straight line up from its king
 * \return 2 if the player is in check from a straight line down from its king
 * \return 3 if the player is in check from a straight line left from its king
 * \return 4 if the player is in check from a straight line right from its king
 * \return 5 if the player is in check from a diagonal up left from its king
 * \return 6 if the player is in check from a diagonal down right from its king
 * \return 7 if the player is in check from a diagonal up right from its king
 * \return 8 if the player is in check from a diagonal doyn left from its king
 * \return the corresponding value x10 if the piece does the check
 * \return 100 if a knight checks
 * values are to be summed if many cases happen at the same time
// TODO : castling
 */
unsigned char CheckCheck( const Board* b , const Move last_move_played, PieceAndPosition king );
Move chIAssWaitForOpponent(void);

std::vector<Move> ParseOpening( FILE* f );
fast32 BoardValue( const Board* b , std::vector<PieceAndPosition> your_pieces, std::vector<PieceAndPosition> oppo_pieces );

ValuedMove chIAssMove( Board* b, fast8 current_depth, fast8 expected_depth, 
std::vector<PieceAndPosition> your_pieces, std::vector<PieceAndPosition> opponents_pieces, 
Move last_move_played );


bool chIAss_v1( Board b, std::string opening, u8 colour , u8 depth);
#endif //CHIASS_V1_HH