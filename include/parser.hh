#ifndef PARSER_HH
#define PARSER_HH

#include "chess.hh"

std::vector<std::vector<Move>> ParseOpening( const opening_name, const unsigned char deepth );

#endif // PARSER_HH