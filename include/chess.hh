#ifndef CHESS_HH
#define CHESS_HH

#include <cstdio>
// used for the unit testing with CUTE
#include <iostream>
// memset
#include <cstring>
#include <vector>

#include <cstdint>
typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef int_fast8_t fast8;
typedef int_fast16_t fast16;
typedef int_fast32_t fast32;
typedef int_fast64_t fast64;
typedef uint_fast8_t ufast8;
typedef uint_fast16_t ufast16;
typedef uint_fast32_t ufast32;
typedef uint_fast64_t ufast64;

//sleep
#include <unistd.h>


/*
 * Values corresponding to the different pieces
 * The king has 2 distinct values : one if he can castle, one if he can't anymore
 */
enum type_value : u8  { EMPTY=0, PAWN=1, KNIGHT=2, BISHOP=3, ROOK=4, QUEEN=5, KING=6, KING_CASTLED_RIGHT=7, KING_CASTLED_LEFT=8, KING_CASTLED=9 };
enum square : u8 {  a1=0, b1=1, c1=2, d1=3, e1=4, f1=5, g1=6, h1=7,
a2=8, b2=9, c2=10, d2=11, e2=12, f2=13, g2=14, h2=15,
a3=16, b3=17, c3=18, d3=19, e3=20, f3=21, g3=22, h3=23,
a4=24, b4=25, c4=26, d4=27, e4=28, f4=29, g4=30, h4=31,
a5=32, b5=33, c5=34, d5=35, e5=36, f5=37, g5=38, h5=39,
a6=40, b6=41, c6=42, d6=43, e6=44, f6=45, g6=46, h6=47,
a7=48, b7=49, c7=50, d7=51, e7=52, f7=53, g7=54, h7=55,
a8=56, b8=57, c8=58, d8=59, e8=60, f8=61, g8=62, h8=63,
crw=101, clw=102, crb=103, clb=104};
enum letters : u8 { A=0, B=1, C=2, D=3, E=4, F=5, G=6, H=7 };
enum colour : u8 { WHITE, BLACK };

/*
 * Basic structure corresponding to a piece of chess
 * the id of each piece is described in type_value
 * */
typedef struct _piece {
	type_value piece:7;
	u8 colour:1;
} Piece;

typedef struct _piece_position {
	type_value piece:7;
	u8 colour:1;
	square position;
} PieceAndPosition;


/*
 * \brief Basically a board of chess
 * this versions uses an array or arrays, instead of a 1-D array
 * Since the size is known in advance, there won't be realloc, 
 * therefore the arrays should generally be contiguous in memory
 */
typedef struct _ptr_board {
	Piece B[8][8];
} PtrBoard;
/*
 * \brief Basically a board of chess
 * The 0th element is the a1 case
 * The 1st element is the a2
 * It goes on, until the 63th, which is h8
 * You access the e7 case with chess_board[e7]
 */
typedef struct _board {
	Piece B[64];
} Board;

/*
 * \brief Basic structure representing a move
 * \params beg corresponds to the case of the piece that's moving
 * \params end corresponds to the cases it arrives
 * If the first attribute is equal to 101, that represents a WHITE LEFT Castling move
 * If the first attribute is equal to 102, that represents a WHITE RIGHT Castling move
 * If the first attribute is equal to 103, that represents a BLACK LEFT Castling move
 * If the first attribute is equal to 104, that represents a BLACK RIGHT Castling move
 * If end is strictly greater than 63, that represents a promotion 
 * The end case of that move is contained in : end%8 (+56 if white)
 * The value of that promotion is contained in : end/63
 * 8 for a QUEEN
 * 9 for a KNIGHT
 * 10 for a ROOK ( shouldn't happen, ever)
 * 11 for a BISHOP ( shouldn't happen, ever)
 */
class Move {
	public:
		square beg;
		square end;
};
inline bool operator==(const Move& m1, const Move& m2) {
	return m1.beg == m2.beg && m1.end == m2.end;
}
std::ostream& operator<<(std::ostream& os, const Move& m);


void EmptyBoard(Board* b);
void InitBoard(Board* b);
void InitBoard(PtrBoard b);
void SetPosition(Board* b, const std::vector<PieceAndPosition> pieces);
void SetPosition(Board* b, const std::vector<PieceAndPosition> pieces_w , const std::vector<PieceAndPosition> pieces_b );

void DisplayBoard( const Board b );
void DisplayBoard( PtrBoard b );


void UpdateBoard( Board* b , const Move m );
void UpdateBoard( Board* b , const std::vector<Move> m );
PieceAndPosition Play( Board* b , Move m );

#endif // CHESS_HH