#include "chess.hh"


int main( void ) {
    Board bb;
    InitBoard( &bb ) ;
    DisplayBoard( bb );

    fprintf(stdout , "Opening e4...\n");
    Move m = { e2 , e4 };
    UpdateBoard( &bb , m );
    DisplayBoard( bb );
    return 0;
}