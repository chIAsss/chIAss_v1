#include "chess.hh"

void EmptyBoard(Board* b) {
	memset( b->B , 0 , 64 ); 
}
/*
 * Passing a pointer is necessary since C doesn't treat array like pointers (: (: (: (: (. (.
 */
void InitBoard(Board* b) {
	memset( b->B , 0 , 64 ); 
	b->B[a1] = {ROOK, WHITE};
	b->B[b1] = {KNIGHT, WHITE};
	b->B[c1] = {BISHOP, WHITE};
	b->B[d1] = {QUEEN, WHITE};
	b->B[e1] = {KING, WHITE};
	b->B[f1] = {BISHOP, WHITE};
	b->B[g1] = {KNIGHT, WHITE};
	b->B[h1] = {ROOK, WHITE};
	b->B[a2] = {PAWN, WHITE};
	b->B[b2] = {PAWN, WHITE};
	b->B[c2] = {PAWN, WHITE};
	b->B[d2] = {PAWN, WHITE};
	b->B[e2] = {PAWN, WHITE};
	b->B[f2] = {PAWN, WHITE};
	b->B[g2] = {PAWN, WHITE};
	b->B[h2] = {PAWN, WHITE};

// Black
	b->B[a8] = {ROOK, BLACK};
	b->B[b8] = {KNIGHT, BLACK};
	b->B[c8] = {BISHOP, BLACK};
	b->B[d8] = {QUEEN, BLACK};
	b->B[e8] = {KING, BLACK};
	b->B[f8] = {BISHOP, BLACK};
	b->B[g8] = {KNIGHT, BLACK};
	b->B[h8] = {ROOK, BLACK};
	b->B[a7] = {PAWN, BLACK};
	b->B[b7] = {PAWN, BLACK};
	b->B[c7] = {PAWN, BLACK};
	b->B[d7] = {PAWN, BLACK};
	b->B[e7] = {PAWN, BLACK};
	b->B[f7] = {PAWN, BLACK};
	b->B[g7] = {PAWN, BLACK};
	b->B[h7] = {PAWN, BLACK};
}

/*
 * You don't need to pass a pointer since the size of the board is known in advance
 * You won't realloc
 */
void InitBoard(PtrBoard b) {
	memset( b.B[0] , 0 , 8 );
	memset( b.B[1] , 0 , 8 );
	memset( b.B[2] , 0 , 8 );
	memset( b.B[3] , 0 , 8 );
	memset( b.B[4] , 0 , 8 );
	memset( b.B[5] , 0 , 8 );
	memset( b.B[6] , 0 , 8 );
	memset( b.B[7] , 0 , 8 ); 
	b.B[A][0] = {ROOK, WHITE};
	b.B[B][0] = {KNIGHT, WHITE};
	b.B[C][0] = {BISHOP, WHITE};
	b.B[D][0] = {QUEEN, WHITE};
	b.B[E][0] = {KING, WHITE};
	b.B[F][0] = {BISHOP, WHITE};
	b.B[G][0] = {KNIGHT, WHITE};
	b.B[H][0] = {ROOK, WHITE};
	b.B[A][1] = {PAWN, WHITE};
	b.B[B][1] = {PAWN, WHITE};
	b.B[C][1] = {PAWN, WHITE};
	b.B[D][1] = {PAWN, WHITE};
	b.B[E][1] = {PAWN, WHITE};
	b.B[F][1] = {PAWN, WHITE};
	b.B[G][1] = {PAWN, WHITE};
	b.B[H][1] = {PAWN, WHITE};

// Black
	b.B[A][7] = {ROOK, BLACK};
	b.B[B][7] = {KNIGHT, BLACK};
	b.B[C][7] = {BISHOP, BLACK};
	b.B[D][7] = {QUEEN, BLACK};
	b.B[E][7] = {KING, BLACK};
	b.B[F][7] = {BISHOP, BLACK};
	b.B[G][7] = {KNIGHT, BLACK};
	b.B[H][7] = {ROOK, BLACK};
	b.B[A][6] = {PAWN, BLACK};
	b.B[B][6] = {PAWN, BLACK};
	b.B[C][6] = {PAWN, BLACK};
	b.B[D][6] = {PAWN, BLACK};
	b.B[E][6] = {PAWN, BLACK};
	b.B[F][6] = {PAWN, BLACK};
	b.B[G][6] = {PAWN, BLACK};
	b.B[H][6] = {PAWN, BLACK};
}

/*
 * The Board is automatically cleaned, so you don't have to call EmptyBoard before setting the positions
 */
void SetPosition(Board* b, const std::vector<PieceAndPosition> pieces ) {
	memset( b->B , 0 , 64 ); 
	for ( PieceAndPosition p : pieces ) {
		b->B[ p.position ] = { p.piece , p.colour };	
	}
}
/*
 * The Board is automatically cleaned, so you don't have to call EmptyBoard before setting the positions
 */
void SetPosition(Board* b, const std::vector<PieceAndPosition> pieces_w, const std::vector<PieceAndPosition> pieces_b ) {
	memset( b->B , 0 , 64 ); 
	for ( PieceAndPosition p : pieces_w ) {
		b->B[ p.position ] = { p.piece , p.colour };	
	} 
	for ( PieceAndPosition p : pieces_b ) {
		b->B[ p.position ] = { p.piece , p.colour };	
	}
}



void DisplayBoard( const Board b ) {
	for ( char i = 7 ; i >= 0 ; i-- ) {
		for ( char j = 0 ; j < 8 ; j++ ) {
			switch ( b.B[ 8*i + j ].piece ) {
				case ROOK:
					fprintf( stdout , "R" );
					break;
				case KNIGHT:
					fprintf( stdout , "N" );
					break;
				case BISHOP:
					fprintf( stdout , "B" );
					break;
				case QUEEN:
					fprintf( stdout , "Q" );
					break;
				case KING:
				case KING_CASTLED:
				case KING_CASTLED_RIGHT:
				case KING_CASTLED_LEFT:
					fprintf( stdout , "K" );
					break;
				case PAWN:
					fprintf( stdout , "P" );
					break;
				default :
					fprintf( stdout , "OO");
			}
			if ( b.B[ 8*i + j ].piece != EMPTY ) {
				switch ( b.B[ 8*i + j ].colour ) {
					case WHITE:
						fprintf( stdout , "w" );
						break;
					case BLACK:
						fprintf( stdout , "b" );
						break;
				}
			}
			fprintf( stdout , "\t");

		}
		fprintf( stdout , "\n" );
	}
}
void DisplayBoard( PtrBoard b );


void UpdateBoard( Board* b , const Move m ) {
	b->B[ m.end ] = b->B[ m.beg ];
	b->B[ m.beg ] = { EMPTY, WHITE };
}
void UpdateBoard( Board* b , const std::vector<Move> m ) {
	for ( Move mov : m ) {
		b->B[ mov.end ] = b->B[ mov.beg ];
		b->B[ mov.beg ] = { EMPTY, WHITE };
	}
}


PieceAndPosition Play( Board* b , Move m ) {
	type_value took;
	u8 c;
	if ( m.end < 64 ) {// Normal move
		took = b->B[ m.end ].piece;
		c = b->B[ m.end ].colour;
		b->B[ m.end ] = b->B[ m.beg ];
		b->B[ m.beg ] = { EMPTY, WHITE };
		return {took, c, (square) m.end};
	} else if ( m.end < 100 ){// Promotion
		square actual_end;
		switch ( b->B[m.beg].colour ) {
			case WHITE:
				actual_end = (square) ( m.end%63 + 56 );
				took = b->B[actual_end].piece;
				c = b->B[ actual_end ].colour;
				break;
			case BLACK:
				actual_end = (square) ( m.end%63 ) ;
				c = b->B[actual_end].colour;
				took = b->B[actual_end].piece;
				break;
		}
		switch ( m.end/63 ) {
			case 8:// QUEEN promotion
				switch ( b->B[m.beg].colour ) {
					case WHITE:
						b->B[m.end%63 + 56] = {QUEEN, WHITE};
						break;
					case BLACK:
						b->B[m.end%63] = {QUEEN, BLACK};
						break;
				}
				break;
			case 9:// KNIGHT promotion
				switch ( b->B[m.beg].colour ) {
					case WHITE:
						b->B[m.end%63 + 56] = {KNIGHT, WHITE};
						break;
					case BLACK:
						b->B[m.end%63] = {KNIGHT, BLACK};
						break;
				}
				break;
			case 10:// CASE ROOK, shouldn't exist
				switch ( b->B[m.beg].colour ) {
					case WHITE:
						b->B[m.end%63 + 56] = {ROOK, WHITE};
						break;
					case BLACK:
						b->B[m.end%63] = {ROOK, BLACK};
						break;
				}
				break;
			case 11:// CASE BISHOP, shouldn't exist
				switch ( b->B[m.beg].colour ) {
					case WHITE:
						b->B[m.end%63 + 56] = {BISHOP, WHITE};
						break;
					case BLACK:
						b->B[m.end%63] = {BISHOP, BLACK};
						break;
				}
				break;
		}
		b->B[ m.beg ] = { EMPTY, WHITE };
		return {took, c, actual_end};

	} else {// Castle
		square actual_end;
		switch ( (u8) m.end ) {
			case 101:// WHITE LEFT 
				b->B[0] = { EMPTY, WHITE };
				b->B[4] = { EMPTY, WHITE };
				b->B[2] = { KING_CASTLED, WHITE };
				b->B[3] = { ROOK, WHITE };
				actual_end = (square) 3;
				c = WHITE;
				break;
			case 102:// WHITE RIGHT 
				b->B[4] = { EMPTY, WHITE };
				b->B[7] = { EMPTY, WHITE };
				b->B[6] = { KING_CASTLED, WHITE };
				b->B[5] = { ROOK, WHITE };
				c = WHITE;
				actual_end = (square) 5;
				break;
			case 103:// BLACK LEFT 
				b->B[60] = { EMPTY, WHITE };
				b->B[56] = { EMPTY, WHITE };
				b->B[58] = { KING_CASTLED, BLACK };
				b->B[59] = { ROOK, BLACK };
				c = BLACK;
				actual_end = (square) 59;
				break;
			case 104:// BLACK RIGHT 
				b->B[60] = { EMPTY, WHITE };
				b->B[63] = { EMPTY, WHITE };
				b->B[62] = { KING_CASTLED, BLACK };
				b->B[61] = { ROOK, BLACK };
				c = BLACK;
				actual_end = (square) 61;
				break;
			default:
				return { (type_value) 0 , (colour) 0 , (square) 0  };
		}
		return { (type_value) 0 , c , actual_end };
	}
	return { (type_value) 0  , (colour) 0 , (square) 0 };
}


std::ostream& operator<<(std::ostream& os, const Move& m) {
    os << static_cast<char>( m.beg%8 +97 ) << (int) m.beg/8 +1<<',' << static_cast<char>( m.end%8+97 ) << (int)m.end/8 +1;
    return os;
}