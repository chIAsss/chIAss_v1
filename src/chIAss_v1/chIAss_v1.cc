#include "chiass_v1.hh"

bool chIAss_v1( Board b, std::string opening, u8 colour , u8 depth) {
    FILE* opening_file = NULL;
    if ( opening.size() != 0 ) { 
        opening_file = fopen( ("doc/" + opening + ".opening").c_str() , "r" );
        if ( opening_file == NULL ) {
            opening_file = fopen( ("doc/" + opening).c_str() , "r" );
            if ( opening_file == NULL ) {
                opening_file = fopen( opening.c_str() , "r" );
                if ( opening_file == NULL ) {
                    opening_file = fopen( "doc/standard.opening" , "r" );
                }
            }
        }
    }
    Move opponent_move;
// Opening phase =============================================================================================
    std::vector<Move> opening_moves;
    if ( opening_file != NULL ) {
        printf("reading the opening...\n");
        opening_moves = ParseOpening( opening_file );
    } else {
        if ( colour == WHITE ) {
            opening_moves = { {e2,e4}, {g1,f3},{f1,c4}};
        } else {
            opening_moves = { {e7,e5}, {b8,c6},{f7,f6} };
        }
    }
    if ( colour == WHITE ) {
        Play( &b, opening_moves[0] ); 
    }
    else {
        opponent_move = chIAssWaitForOpponent();
        Play( &b, opponent_move);
        Play( &b, opening_moves[0] ); 
    }
    DisplayBoard(b);
    for ( size_t i = 1 ; i < opening_moves.size() ; i++ ) {
        opponent_move = chIAssWaitForOpponent();
        Play( &b, opponent_move);
        Play( &b, opening_moves[i] );
        DisplayBoard(b);
    }

    opponent_move = chIAssWaitForOpponent();
    Play( &b, opponent_move);

    DisplayBoard(b);
// End of the opening phase ==================================================================================
    std::vector<PieceAndPosition> your_pieces;
    std::vector<PieceAndPosition> opponent_pieces;

    for ( unsigned char i = 0 ; i < 64 ; i++ ) {
        PieceAndPosition added_piece;
		if ( b.B[i].piece != EMPTY ) {
            added_piece.piece = b.B[i].piece;
            added_piece.colour = b.B[i].colour;
            added_piece.position = (square) i;
            switch ( b.B[i].colour == colour ) {
                case true:
                    your_pieces.push_back( added_piece );
                    break;
                case false:
                    opponent_pieces.push_back( added_piece );
            }
    // the king is in first place of the vector
            if ( b.B[i].piece >= KING ) {
                PieceAndPosition temporary;
                switch (  b.B[i].colour == colour ) {
                    case true:
                        temporary = your_pieces[0];
                        your_pieces[0] = your_pieces.back();
                        your_pieces.back() = temporary;
                        break;
                    case false:
                        temporary = opponent_pieces[0];
                        opponent_pieces[0] = opponent_pieces.back();
                        opponent_pieces.back() = temporary;
                }
            }
        }
    }

    ValuedMove m =  chIAssMove( &b, 0, depth,
    your_pieces, opponent_pieces,
    opponent_move);
// The piece that's been taken, if ever
    PieceAndPosition played;

    while ( m.m.beg != 0 || m.m.end != 0 ) {
// chIAss plays its move
        played = Play( &b, m.m );
        std::cout << m<<"\n";
        DisplayBoard(b);
        fprintf(stdout, "\n\n");
        if ( played.piece != EMPTY ) {
            for ( ufast8 j = 0 ; j < opponent_pieces.size() ; j++ ) {
                if ( played.position == opponent_pieces[j].position ) {
                    opponent_pieces[j] = opponent_pieces.back();
                    opponent_pieces.pop_back();
                    break;
                }
            }
        }
// updating the piece position in the vector
        for ( ufast8 j = 0 ; j < your_pieces.size() ; j++ ) {
            if ( m.m.beg == your_pieces[j].position ) {
                your_pieces[j].position = m.m.end;
                break;
            }
        }

        opponent_move = chIAssWaitForOpponent();
        played = Play( &b, opponent_move );
        DisplayBoard(b);
        fprintf(stdout, "\n\n");
        if ( played.piece != EMPTY ) {
            for ( ufast8 j = 0 ; j < your_pieces.size() ; j++ ) {
                if ( played.position == your_pieces[j].position ) {
                    your_pieces[j] = your_pieces.back();
                    your_pieces.pop_back();
                    break;
                }
            }
        }
// updating the piece position in the vector
        for ( ufast8 j = 0 ; j < opponent_pieces.size() ; j++ ) {
            if ( opponent_move.beg == opponent_pieces[j].position ) {
                opponent_pieces[j].position = opponent_move.end;
                break;
            }
        }

        m = chIAssMove( &b, 0, depth  , your_pieces, opponent_pieces, opponent_move );
    }
    return true;
}