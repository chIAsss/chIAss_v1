#include "chiass_v1.hh"

bool ValidKingMove( const Board b , const Move king ) {
// UP
	u8 checking_square = king.end + 8;
	while ( checking_square < 64 ) {
		if ( b.B[ checking_square ].piece != EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king.end].colour ) {
				if ( b.B[ checking_square ].piece == ROOK ||
				b.B[ checking_square ].piece == QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square += 8;
	}
// DOWN
	checking_square = king.end - 8;
	while ( checking_square < 64 ) {
		if ( b.B[ checking_square ].piece != EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king.end].colour ) {
				if ( b.B[ checking_square ].piece == ROOK ||
				b.B[ checking_square ].piece == QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square -= 8;
	}
// LEFT
	checking_square = king.end + 1;
	while ( checking_square%8 != 0 ) {
		if ( b.B[ checking_square ].piece != EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king.end].colour ) {
				if ( b.B[ checking_square ].piece == ROOK ||
				b.B[ checking_square ].piece == QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square ++;
	}
// RIGHT
	checking_square = king.end - 1;
	while ( checking_square%8 != 7 ) {
		if ( b.B[ checking_square ].piece != EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king.end].colour ) {
				if ( b.B[ checking_square ].piece == ROOK ||
				b.B[ checking_square ].piece == QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square --;
	}

// UP LEFT
	checking_square = king.end + 7;
	while ( checking_square < 64 && checking_square%8 != 0 ) {
		if ( b.B[ checking_square ].piece != EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king.end].colour ) {
				if ( b.B[ checking_square ].piece == BISHOP ||
				b.B[ checking_square ].piece == QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square += 7;
	}
// DOWN RIGHT
	checking_square = king.end - 7;
	while ( checking_square < 64 && checking_square%8 != 7 ) {
		if ( b.B[ checking_square ].piece != EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king.end].colour ) {
				if ( b.B[ checking_square ].piece == BISHOP ||
				b.B[ checking_square ].piece == QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square -= 7;
	}
// UP RIGHT
	checking_square = king.end + 9;
	while ( checking_square < 64 && checking_square%8 != 0 ) {
		if ( b.B[ checking_square ].piece != EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king.end].colour ) {
				if ( b.B[ checking_square ].piece == BISHOP ||
				b.B[ checking_square ].piece == QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square += 9;
	}
// DOWN LEFT
	checking_square = king.end - 9;
	while ( checking_square < 64 && checking_square%8 != 7 ) {
		if ( b.B[ checking_square ].piece != EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king.end].colour ) {
				if ( b.B[ checking_square ].piece == BISHOP ||
				b.B[ checking_square ].piece == QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square -= 9;
	}

// checking for KNIGHT
	if ( king.end < 48 ) {
		if ( king.end%8 > 1 ) {
			if ( b.B[ king.end + 6 ].piece == KNIGHT && b.B[ king.end + 6 ].colour != b.B[king.end].colour ) {
// first square
				return false;
			}
			if ( b.B[ king.end + 15 ].piece == KNIGHT && b.B[ king.end + 15 ].colour != b.B[king.end].colour ) {
// second square 
				return false;
			}

			if ( king.end%8 < 6 ) {
				if ( b.B[ king.end + 17 ].piece == KNIGHT && b.B[ king.end + 17 ].colour != b.B[king.end].colour ) {
// third square
					return false;
				}
				if ( b.B[ king.end + 10 ].piece == KNIGHT && b.B[ king.end + 10 ].colour != b.B[king.end].colour ) {
// fourth square 
					return false;
				}
			} else if ( king.end%8 == 6 ) {
				if ( b.B[ king.end + 17 ].piece == KNIGHT && b.B[ king.end + 17 ].colour != b.B[king.end].colour ) {
// third square
					return false;
				}
			}

		} else {// king.end%8 <= 1
			if ( king.end%8 == 1 ) { //  if not > 1, then > 0 is equivalent to == 1
				if ( b.B[ king.end + 15 ].piece == KNIGHT && b.B[ king.end + 15 ].colour != b.B[king.end].colour ) {
// second square 
					return false;
				}
			}
			// king.end%8 <= 1 implies that king.end%8 < 6
			if ( b.B[ king.end + 17 ].piece == KNIGHT && b.B[ king.end + 17 ].colour != b.B[king.end].colour ) {
// third square
				return false;
			}
			if ( b.B[ king.end + 10 ].piece == KNIGHT && b.B[ king.end + 10 ].colour != b.B[king.end].colour ) {
// fourth square 
				return false;
			}
		}
		if ( king.end > 15 ) {
			if ( king.end%8 > 1 ) {
				if ( king.end%8 < 6 ) {
					if ( b.B[ king.end - 6 ].piece == KNIGHT && b.B[ king.end - 6 ].colour != b.B[king.end].colour ) {
// fifth square 
						return false;
					}
					if ( b.B[ king.end - 15 ].piece == KNIGHT && b.B[ king.end - 15 ].colour != b.B[king.end].colour ) {
// sixth square 
						return false;
					}
				} else if ( king.end%8 == 6 ) {// if not > 1, then < 7 is equivalent to == 6
					if ( b.B[ king.end - 15 ].piece == KNIGHT && b.B[ king.end - 15 ].colour != b.B[king.end].colour ) {
// sixth square 
						return false;
					}
				}
				if ( b.B[ king.end - 17 ].piece == KNIGHT && b.B[ king.end - 17 ].colour != b.B[king.end].colour ) {
// seventh square
					return false;
				}
				if ( b.B[ king.end - 10 ].piece == KNIGHT && b.B[ king.end - 10 ].colour != b.B[king.end].colour ) {
// eighth square 
					return false;
				}

			} else {
				// king.end%8 <= 1 implies king.end%8 < 6
				if ( b.B[ king.end - 6 ].piece == KNIGHT && b.B[ king.end - 6 ].colour != b.B[king.end].colour ) {
// fifth square 
					return false;
				}
				if ( b.B[ king.end - 15 ].piece == KNIGHT && b.B[ king.end - 15 ].colour != b.B[king.end].colour ) {
// sixth square 
					return false;
				}
				if ( king.end%8 == 1 ) {// if not > 1, then > 0 is equivalent to == 1
					if ( b.B[ king.end - 17 ].piece == KNIGHT && b.B[ king.end - 17 ].colour != b.B[king.end].colour ) {
// seventh square 
						return false;
					
					}
				}
			}


		} else if ( king.end > 7 ) { // only the 5th and 8th moves are possible
			if ( king.end%8 < 6 ) {
				if ( b.B[ king.end - 6 ].piece == KNIGHT && b.B[ king.end - 6 ].colour != b.B[king.end].colour ) {
// fifth square 
					return false;
				}
				if ( king.end%8 > 1 ) {
					if ( b.B[ king.end - 10 ].piece == KNIGHT && b.B[ king.end - 10 ].colour != b.B[king.end].colour ) {
// eighth square 
						return false;
					}
				}
			} else {// king.end%8 >= 6, then it's also > 1 , the 8th move is necessarily an option
				if ( b.B[ king.end - 10 ].piece == KNIGHT && b.B[ king.end - 10 ].colour != b.B[king.end].colour ) {
// eighth square 
					return false;
				}
			}
		}
	} else { // position >= 48, so it's > 15, only have to check for 5th through 8th moves
		if ( king.end%8 > 1 ) {
			if ( king.end%8 < 6 ) {
				if ( b.B[ king.end - 6 ].piece == KNIGHT && b.B[ king.end - 6 ].colour != b.B[king.end].colour ) {
// fifth square 
					return false;
				}
				if ( b.B[ king.end - 15 ].piece == KNIGHT && b.B[ king.end - 15 ].colour != b.B[king.end].colour ) {
// sixth square 
					return false;
				}
			} else if ( king.end%8 == 6 ) {// if not > 1, then < 7 is equivalent to == 6
				if ( b.B[ king.end - 15 ].piece == KNIGHT && b.B[ king.end - 15 ].colour != b.B[king.end].colour ) {
// sixth square 
					return false;
				}
			}
			if ( b.B[ king.end - 17 ].piece == KNIGHT && b.B[ king.end - 17 ].colour != b.B[king.end].colour ) {
// seventh square
				return false;
			}
			if ( b.B[ king.end - 10 ].piece == KNIGHT && b.B[ king.end - 10 ].colour != b.B[king.end].colour ) {
// eighth square 
				return false;
			}

		} else {
			// king.end%8 <= 1 implies king.end%8 < 6
			if ( b.B[ king.end - 6 ].piece == KNIGHT && b.B[ king.end - 6 ].colour != b.B[king.end].colour ) {
// fifth square 
				return false;
			}
			if ( b.B[ king.end - 15 ].piece == KNIGHT && b.B[ king.end - 15 ].colour != b.B[king.end].colour ) {
// sixth square 
				return false;
			}
			if ( king.end%8 == 1 ) {// if not > 1, then > 0 is equivalent to == 1
				if ( b.B[ king.end - 17 ].piece == KNIGHT && b.B[ king.end - 17 ].colour != b.B[king.end].colour ) {
// seventh square 
					return false;
				
				}
			}
		}
	}
// checking for PAWN
	switch ( b.B[king.end].colour ) {
		case WHITE:
			if ( b.B[ king.end + 7 ].piece == PAWN &&
			b.B[ king.end + 7 ].colour == BLACK ) {
				return false;
			}
			if ( b.B[ king.end + 9 ].piece == PAWN &&
			b.B[ king.end + 9 ].colour == BLACK ) {
				return false;
			}
			break;
		case BLACK:
			if ( b.B[ king.end - 7 ].piece == PAWN &&
			b.B[ king.end - 7 ].colour == WHITE ) {
				return false;
			}
			if ( b.B[ king.end - 9 ].piece == PAWN &&
			b.B[ king.end - 9 ].colour == WHITE ) {
				return false;
			}
			break;
	}

// Checking for KING
    if ( king.end%8 > 0 ) {
        if ( b.B[ king.end - 1 ].piece >= KING ) {
            return false;
        }
        if ( king.end < 56 ) {
            if ( b.B[ king.end + 7 ].piece >= KING ) {
                return false;
            }
            if ( b.B[ king.end + 8 ].piece >= KING ) {
                return false;
            }
            if ( king.end > 7 ) {
                if ( b.B[ king.end - 8 ].piece >= KING ) {
                    return false;
                }
                if ( b.B[ king.end - 9 ].piece >= KING ) {
                    return false;
                }
                if ( king.end%8 < 7 ) {
                    if ( b.B[ king.end - 7 ].piece >= KING ) {
                        return false;
                    }
                    if ( b.B[ king.end + 1 ].piece >= KING ) {
                        return false;
                    }
                    if ( b.B[ king.end + 9 ].piece >= KING ) {
                        return false;
                    }
                }
            } else {
                if ( king.end%8 < 7 ) {
                    if ( b.B[ king.end + 1 ].piece >= KING ) {
                        return false;
                    }
                    if ( b.B[ king.end + 9 ].piece >= KING ) {
                        return false;
                    }
                }
            }
        } else {
            if ( b.B[ king.end - 8 ].piece >= KING ) {
                return false;
            }
            if ( b.B[ king.end - 9 ].piece >= KING ) {
                return false;
            }
            if ( king.end%8 < 7 ) {
                if ( b.B[ king.end - 7 ].piece >= KING ) {
                    return false;
                }
                if ( b.B[ king.end + 1 ].piece >= KING ) {
                    return false;
                }
            }
        }
    } else {
        if ( b.B[ king.end + 1 ].piece >= KING ) {
            return false;
        }
        if ( king.end < 56 ) {
            if ( b.B[ king.end + 8 ].piece >= KING ) {
                return false;
            }
            if ( b.B[ king.end + 9 ].piece >= KING ) {
                return false;
            }
            
            if ( king.end > 7 ) {
                if ( b.B[ king.end - 8 ].piece >= KING ) {
                    return false;
                }
                if ( b.B[ king.end - 7 ].piece >= KING ) {
                    return false;
                }
            }
        } else {
            if ( b.B[ king.end - 8 ].piece >= KING ) {
                return false;
            }
            if ( b.B[ king.end - 7 ].piece >= KING ) {
                return false;
            }
        }
    }

	return true;
}