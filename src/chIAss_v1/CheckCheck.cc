#include "chiass_v1.hh"


unsigned char CheckCheck( const Board* b, const Move last_move_played, PieceAndPosition king ) {
	unsigned char checking_square;
	unsigned char returned_check_value = 0;

// The principe is always the same : 
// first, we check if the king is in line of sight of where the piece was before its move
// -> if it is, then there's a chance that this move causes a check, by freeing the line of sight of a piece that was behind
// If the king is in the line of sight of the starting case of the move
// then we first verify if there's a piece, that was "behind" ( the meaning of behind depends of the type of line : straight or diagonal )
// if a piece is behind, we verify if it could use that new line of sight ( bishop can't attack straight (: )
// then, if it can, we verify if the line of sight is actually free

// Finally we check if the piece checks the king after its move
/*	printf("last move : %d %d\n", last_move_played.beg, last_move_played.end);
	printf("king position : %d\n", king.position);*/
	switch ( b->B[last_move_played.end].piece ) {
		case QUEEN:
			if ( last_move_played.end%8 == king.position%8 ) {// the queen is aligned up/down with the king
				if ( last_move_played.end > king.position ) {// the danger is UP from the king
					// checking for a line of sight to the king
					checking_square = last_move_played.end - 8;
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 10 ;
							}
							break;
						}
						checking_square -= 8;
					}
				} else {// the danger is DOWN from the king
				// checking for a line of sight to the king
					checking_square = last_move_played.end + 8;
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 20;
							}
							break;
						}
						checking_square += 8;
					}
				}
			} else if ( last_move_played.end/8 == king.position/8 ) {// the quen is aligned left/right with the king
				if ( last_move_played.end%8 > king.position%8 ) {// the queen is right, we check left from her
					checking_square = (unsigned char) ( last_move_played.end - 1 ) ;
			// as long as the case is in the board : not too much on the right ( which would make it pop on the left )
					while ( checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 40;
							}
							break;
						}
						checking_square --;
					}
				} else {// the queen is left, we check right from her
					checking_square = (unsigned char) ( last_move_played.end + 1 );
					while ( checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 30;
							}
							break;
						}
						checking_square ++;
					}
				}
			} else if ( last_move_played.end/8 + last_move_played.end%8 == king.position/8 + king.position%8 ) { // up left / down right
				if ( last_move_played.end > king.position ) { // UP LEFT
				checking_square = (unsigned char) ( last_move_played.end - 7 );
	// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
				while ( checking_square < 64 && checking_square%8 != 0 ) {
					if ( b->B[ checking_square ].piece != EMPTY ) {
						if ( checking_square == king.position ) {
							returned_check_value = 50;
						}
						break;
					}
					checking_square -= 7;
				}
				} else { // DOWN RIGHT
					checking_square = (unsigned char) ( last_move_played.end + 7 );
// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 60;
							}
							break;
						}
						checking_square += 7;
					}
				}

			} else if ( last_move_played.end/8 + 8 - last_move_played.end%8 == king.position/8 + 8 - king.position%8 ) { // up right / down left
				if ( last_move_played.end > king.position ) { // UP RIGHT
					checking_square = (unsigned char) (last_move_played.end - 9 );
// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 70;
							}
							break;
						}
						checking_square -= 9;
					}
				} else { // DOWN LEFT
					checking_square = (unsigned char) ( last_move_played.end + 9 );
		// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 80;
							}
							break;
						}
						checking_square += 9;
					}
				}
			}
// a QUEEN can free a line of sight ONLY if she's just been promoted
			switch ( b->B[ last_move_played.end ].colour ) {
				case WHITE:
					if ( last_move_played.end/8 == 7 && last_move_played.beg/8 == 6 ) {
						goto every_lines;
					}
					break;
				case BLACK:
					if ( last_move_played.end/8 == 0 && last_move_played.beg/8 == 1 ) {
						goto every_lines;
					}
					break;
			}
			break;

		case PAWN:
			switch ( b->B[ last_move_played.end ].colour ) {
				case WHITE:
					if ( last_move_played.end%8 != 0 && last_move_played.end + 7 == king.position ) {
						if ( last_move_played.beg%8 == last_move_played.end%8 ) {
// if the pawn just advanced and check, it can't have freed a line of sight
							return 60;
						} else {// if the pawn took and checked, it might have freed a straight line of sight
							returned_check_value = 60;
							goto Straight_line_of_sight;
						}
					} 
					if ( last_move_played.end%8 != 7 && last_move_played.end + 9 == king.position ) {
						if ( last_move_played.beg%8 == last_move_played.end%8 ) {
// if the pawn just advanced and check, it can't have freed a line of sight
							return 80;
						} else {// if the pawn took and checked, it might have freed a straight line of sight
							returned_check_value = 80;
							goto Straight_line_of_sight;
						}
					}
					break;
				case BLACK:
					if ( last_move_played.end%8 != 7 &&  last_move_played.end - 7 == king.position ) {
						if ( last_move_played.beg%8 == last_move_played.end%8 ) {
// if the pawn just advanced and check, it can't have freed a line of sight
							return 50;
						} else {// if the pawn took and checked, it might have freed a straight line of sight
							returned_check_value = 50;
							goto Straight_line_of_sight;
						}
					}
					if ( last_move_played.end%8 != 0 && last_move_played.end - 9 == king.position ) {
						if ( last_move_played.beg%8 == last_move_played.end%8 ) {
// if the pawn just advanced and check, it can't have freed a line of sight
							return 70;
						} else {// if the pawn took and checked, it might have freed a straight line of sight
							returned_check_value = 70;
							goto Straight_line_of_sight;
						}
					}
					break;
			}
			goto every_lines;// K--P--R, K-|P-|R
			break;

		case ROOK:
			if ( last_move_played.end%8 == king.position%8 ) {// the rook is aligned up/down with the king
				if ( last_move_played.end > king.position ) {// the danger is UP from the king
					// checking for a line of sight to the king
					checking_square = last_move_played.end - 8;
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 10;
							}
							break;
						}
						checking_square -= 8;
					}
				} else {// the danger is DOWN from the king
				// checking for a line of sight to the king
					checking_square = last_move_played.end + 8;
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 20;
							}
							break;
						}
						checking_square += 8;
					}
				}
			} else if ( last_move_played.end/8 == king.position/8 ) {// the rook is aligned left/right with the king
				if ( last_move_played.end%8 > king.position%8 ) {// the rook is RIGHT, we check left from it
					checking_square = (unsigned char) ( last_move_played.end - 1 ) ;
			// as long as the case is in the board : not too much on the right ( which would make it pop on the left )
					while ( checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 40 ;
							}
							break;
						}
						checking_square --;
					}
				} else {// the rook is LEFT, we check right
					checking_square = (unsigned char) ( last_move_played.end + 1 );
					while ( checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 30 ;
							}
							break;
						}
						checking_square ++;
					}
				}
			}

// when checking lines of sight, we start at move.beg
			//Diagonal_line_of_sight:
				if ( last_move_played.beg/8 + last_move_played.beg%8 == king.position/8 + king.position%8 ) {// diagonal up left
					if ( last_move_played.beg > king.position ) {// the danger is UP from the king
			// Up left
						checking_square = (unsigned char) ( last_move_played.beg + 7 );
			// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
						while ( checking_square < 64 && checking_square%8 != 7 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( b->B[ checking_square ].colour != king.colour &&
								( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
				// Down right
									checking_square = (unsigned char) ( last_move_played.beg - 7 );
				// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
									while ( checking_square < 64 && checking_square%8 != 0 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( checking_square == king.position ) {
												returned_check_value += 5;
											}
											break;
										}
										checking_square -= 7;
									}
								}
								break;
							}
							checking_square += 7;
						}
					} else {// the danger is DOWN from the king
			// DOWN RIGHT
						checking_square = (unsigned char) ( last_move_played.beg - 7 );	
			// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
						while ( checking_square < 64 && checking_square%8 != 0 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( b->B[ checking_square ].colour != king.colour &&
								( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
				// UP LEFT
									checking_square = (unsigned char) ( last_move_played.beg + 7 );
			// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
									while ( checking_square < 64 && checking_square%8 != 7 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( checking_square == king.position ) {
												returned_check_value += 6;
											}
											break;
										}
										checking_square += 7;
									}
								}
								break;
							}
							checking_square -= 7;
						}
					}



				} else if ( last_move_played.beg/8 + 8 - last_move_played.beg%8 == king.position/8 + 8 - king.position%8 ) {// diagonal up right
					if ( last_move_played.beg > king.position ) {// the danger is UP from the king

			// Up right
						checking_square = (unsigned char) ( last_move_played.beg + 9 );
			// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
						while ( checking_square < 64 && checking_square%8 != 0 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( b->B[ checking_square ].colour != king.colour &&
								( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!

			// Down left
									checking_square = (unsigned char) (last_move_played.beg - 9 );
			// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
									while ( checking_square < 64 && checking_square%8 != 7 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( checking_square == king.position ) {
												returned_check_value += 7;
											}
											break;
										}
										checking_square -= 9;
									}


								}
								break;
							}
							checking_square += 9;
						}
					} else {// the danger is DOWN from the king

			// DOWN LEFT
						checking_square = (unsigned char) ( last_move_played.beg - 9 );
			// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
						while ( checking_square < 64 && checking_square%8 != 7 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( b->B[ checking_square ].colour != king.colour &&
								( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
			// UP RIGHT
									checking_square = (unsigned char) (last_move_played.beg + 9 );
			// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
									while ( checking_square < 64 && checking_square%8 != 0 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( checking_square == king.position ) {
												returned_check_value += 8;
											}
											break;
										}
										checking_square += 9;
									}


								}
								break;
							}
							checking_square -= 9;
						}
					}
				}
			return returned_check_value;
			break;

		case BISHOP:
			if ( last_move_played.end/8 + last_move_played.end%8 == king.position/8 + king.position%8 ) { // up left / down right
				if ( last_move_played.end > king.position ) { // UP LEFT
					checking_square = (unsigned char) ( last_move_played.end - 7 );
// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 50;
							}
							break;
						}
						checking_square -= 7;
					}
				} else { // DOWN RIGHT
					checking_square = (unsigned char) ( last_move_played.end + 7 );
// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 60;
							}
							break;
						}
						checking_square += 7;
					}
				}

			} else if ( last_move_played.end/8 + 8 - last_move_played.end%8 == king.position/8 + 8 - king.position%8 ) { // up right / down left
				if ( last_move_played.end > king.position ) { // UP RIGHT
					checking_square = (unsigned char) (last_move_played.end - 9 );
// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 70;
							}
							break;
						}
						checking_square -= 9;
					}
				} else { // DOWN LEFT
					checking_square = (unsigned char) ( last_move_played.end + 9 );
		// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = 80;
							}
							break;
						}
						checking_square += 9;
					}
				}
			}

// when checking loines of sight, we start at move.beg
			Straight_line_of_sight:
				if ( last_move_played.beg%8 == king.position%8 ) {// vertical line
			// we check from the moving piece to the border
					if ( last_move_played.beg > king.position ) {// the piece is up the king
						checking_square = last_move_played.beg + 8;
						// as long as the case is in the board : not too high
						while ( checking_square < 64 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( b->B[ checking_square ].colour != king.colour &&
								( b->B[ checking_square ].piece == ROOK || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!

								// checking for a line of sight to the king
									checking_square = last_move_played.beg - 8;
									while ( checking_square < 64 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( checking_square == king.position ) {
												returned_check_value += 1;
											}
											break;
										}
										checking_square -= 8;
									}
								}
								break;
							}
							checking_square += 8;
						}
					} else {
						checking_square = last_move_played.beg - 8;
						// as long as the case is in the board : not too high
						while ( checking_square < 64 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( b->B[ checking_square ].colour != king.colour && 
								( b->B[ checking_square ].piece == ROOK || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!

								// checking for a line of sight to the king
									checking_square = last_move_played.beg + 8;
									while ( checking_square < 64 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( checking_square == king.position ) {
												returned_check_value += 2;
											}
											break;
										}
										checking_square += 8;
									}
								}
								break;
							}
							checking_square -= 8;
						}
					}
				} else if ( last_move_played.beg/8 == king.position/8 ) {// horizontal line  THE /8 isn't necessary

					if ( last_move_played.beg%8 > king.position%8 ) {// we're looking for a threat situated right from the king
						checking_square = (unsigned char) ( last_move_played.beg + 1 ) ;
				// as long as the case is in the board : not too much on the right ( which would make it pop on the left )
						while ( checking_square%8 != 0 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( b->B[ checking_square ].colour != king.colour &&
								( b->B[ checking_square ].piece == ROOK || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!

									checking_square = (unsigned char) ( last_move_played.beg - 1 );
									while ( checking_square%8 != 7 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( checking_square == king.position ) {
												returned_check_value += 4;
											}
											break;
										}
										checking_square --;
									}


								}
								break;
							}
							checking_square ++;
						}
					} else {// we're looking for a threat situated left from the king
						checking_square = (unsigned char) ( last_move_played.beg - 1 ) ;
				// as long as the case is in the board : not too much on the right ( which would make it pop on the left )
						while ( checking_square%8 != 7 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( b->B[ checking_square ].colour != king.colour &&
								( b->B[ checking_square ].piece == ROOK || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!

									checking_square = (unsigned char) ( last_move_played.beg + 1 );
									while ( checking_square%8 != 0 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( checking_square == king.position ) {
												returned_check_value += 3;
											}
											break;
										}
										checking_square ++;
									}
								}
								break;
							}
							checking_square --;
						}
					}
				}


			return returned_check_value;
			break;


		case KNIGHT:
			if ( last_move_played.end%8 - king.position%8 == 1 ||
			king.position%8 - last_move_played.end%8 == 1 ) {// the KNIGHT is RIGHT/LEFT from the KING
				if ( last_move_played.end/8 - king.position/8 == 2 || 
				king.position/8 - last_move_played.end/8 == 2 ) {// the KNIGHT can be UP/DOWN from the KING
// in that case, the option of taking the KNIGHT doesn't work
					returned_check_value = 100;
				}
			} else if ( last_move_played.end%8 - king.position%8 == 2 ||
			king.position%8 - last_move_played.end%8 == 2 ) {
				if ( last_move_played.end/8 - king.position/8 == 1 || 
				king.position/8 - last_move_played.end/8 == 1 ) {// the KNIGHT can be UP/DOWN from the KING
// in that case, the option of taking the KNIGHT doesn't work
					returned_check_value = 100;
				}
			}
// else checking everything
			//goto every_lines;

// the king can't put in check himself, he can only put another piece in position of a check
		every_lines:
		case KING:
		case KING_CASTLED:
		case KING_CASTLED_RIGHT:
		case KING_CASTLED_LEFT:
// checking for every lines of sight


			if ( last_move_played.beg%8 == king.position%8 ) {// vertical line
		// we check from the moving piece to the border
				if ( last_move_played.beg > king.position ) {// the piece is up the king
					checking_square = last_move_played.beg + 8;
					// as long as the case is in the board : not too high
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( b->B[ checking_square ].colour != king.colour &&
							( b->B[ checking_square ].piece == ROOK || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!

							// checking for a line of sight to the king
								checking_square = last_move_played.beg - 8;
								while ( checking_square < 64 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( checking_square == king.position ) {
											returned_check_value += 1;
										}
										break;
									}
									checking_square -= 8;
								}
							}
							break;
						}
						checking_square += 8;
					}
				} else {
					checking_square = last_move_played.beg - 8;
					// as long as the case is in the board : not too high
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( b->B[ checking_square ].colour != king.colour && 
							( b->B[ checking_square ].piece == ROOK || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!

							// checking for a line of sight to the king
								checking_square = last_move_played.beg + 8;
								while ( checking_square < 64 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( checking_square == king.position ) {
											returned_check_value += 2;
										}
										break;
									}
									checking_square += 8;
								}
							}
							break;
						}
						checking_square -= 8;
					}
				}
			} else if ( last_move_played.beg/8 == king.position/8 ) {// horizontal line

				if ( last_move_played.beg%8 > king.position%8 ) {// we're looking for a threat situated right from the king
					checking_square = (unsigned char) ( last_move_played.beg + 1 ) ;
			// as long as the case is in the board : not too much on the right ( which would make it pop on the left )
					while ( checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( b->B[ checking_square ].colour != king.colour &&
							( b->B[ checking_square ].piece == ROOK || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!

								checking_square = (unsigned char) ( last_move_played.beg - 1 );
								while ( checking_square%8 != 7 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( checking_square == king.position ) {
											returned_check_value += 4;
										}
										break;
									}
									checking_square --;
								}


							}
							break;
						}
						checking_square ++;
					}
				} else {// we're looking for a threat situated left from the king
					checking_square = (unsigned char) ( last_move_played.beg - 1 ) ;
			// as long as the case is in the board : not too much on the right ( which would make it pop on the left )
					while ( checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( b->B[ checking_square ].colour != king.colour &&
							( b->B[ checking_square ].piece == ROOK || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!

								checking_square = (unsigned char) ( last_move_played.beg + 1 );
								while ( checking_square%8 != 0 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( checking_square == king.position ) {
											returned_check_value += 3;
										}
										break;
									}
									checking_square ++;
								}
							}
							break;
						}
						checking_square --;
					}
				}

			} else if ( last_move_played.beg/8 + last_move_played.beg%8 == king.position/8 + king.position%8 ) {// diagonal up left
				if ( last_move_played.beg > king.position ) {// the danger is UP from the king
		// Up left
					checking_square = (unsigned char) ( last_move_played.beg + 7 );
		// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( b->B[ checking_square ].colour != king.colour &&
							( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
			// Down right
								checking_square = (unsigned char) ( last_move_played.beg - 7 );
			// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
								while ( checking_square < 64 && checking_square%8 != 0 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( checking_square == king.position ) {
											returned_check_value += 5;
										}
										break;
									}
									checking_square -= 7;
								}
							}
							break;
						}
						checking_square += 7;
					}
				} else {// the danger is DOWN from the king
		// DOWN RIGHT
					checking_square = (unsigned char) ( last_move_played.beg - 7 );	
		// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( b->B[ checking_square ].colour != king.colour &&
							( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
			// UP LEFT
								checking_square = (unsigned char) ( last_move_played.beg + 7 );
		// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
								while ( checking_square < 64 && checking_square%8 != 7 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( checking_square == king.position ) {
											returned_check_value += 6;
										}
										break;
									}
									checking_square += 7;
								}
							}
							break;
						}
						checking_square -= 7;
					}
				}



			} else if ( last_move_played.beg/8 + 8 - last_move_played.beg%8 == king.position/8 + 8 - king.position%8 ) {// diagonal up right
				if ( last_move_played.beg > king.position ) {// the danger is UP from the king

		// Up right
					checking_square = (unsigned char) ( last_move_played.beg + 9 );
		// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( b->B[ checking_square ].colour != king.colour &&
							( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!

		// Down left
								checking_square = (unsigned char) (last_move_played.beg - 9 );
		// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
								while ( checking_square < 64 && checking_square%8 != 7 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( checking_square == king.position ) {
											returned_check_value += 7;
										}
										break;
									}
									checking_square -= 9;
								}


							}
							break;
						}
						checking_square += 9;
					}
				} else {// the danger is DOWN from the king
		// DOWN LEFT
					checking_square = (unsigned char) ( last_move_played.beg - 9 );
		// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( b->B[ checking_square ].colour != king.colour &&
							( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
		// UP RIGHT
								checking_square = (unsigned char) (last_move_played.beg + 9 );
		// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
								while ( checking_square < 64 && checking_square%8 != 0 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( checking_square == king.position ) {
											returned_check_value += 8;
										}
										break;
									}
									checking_square += 9;
								}
							}
							break;
						}
						checking_square -= 9;
					}
				}
			}
		return returned_check_value;

		default:
			return 0;
	}
	return returned_check_value;

}
