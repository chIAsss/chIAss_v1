#include "chiass_v1.hh"


std::vector<Move> AllPossibleMovesCheck( const Board* b, const std::vector<PieceAndPosition> list_pieces,
Move last_move_played, PieceAndPosition king, u8 result ) {
	std::vector<Move> returned;
	std::vector<Move> all_moves;
    switch ( result ) {
        case 10:
        case 1:// line UP
            all_moves = AllPossibleMoves( b , list_pieces );
            for ( Move mov : all_moves ) {
                if ( b->B[ mov.beg ].piece == KING ||
                ( ( mov.end%8 == king.position%8 ) && 
                ( mov.end > king.position ) && 
                ( mov.end <= last_move_played.end ) ) ) {
                    returned.push_back( mov );
                }
            }
            return returned;
        case 20:
        case 2:// line DOWN
            all_moves = AllPossibleMoves( b , list_pieces );
            for ( Move mov : all_moves ) {
                if ( b->B[ mov.beg ].piece == KING ||
                ( ( mov.end%8 == king.position%8 ) && 
                ( mov.end < king.position ) && 
                ( mov.end >= last_move_played.end ) ) ) {
                    returned.push_back( mov );
                }
            }
            return returned;
        case 30:
        case 3:// line LEFT
            all_moves = AllPossibleMoves( b , list_pieces );
            for ( Move mov : all_moves ) {
                if ( b->B[ mov.beg ].piece == KING ||
                ( ( mov.end/8 == king.position/8 ) && 
                ( mov.end < king.position ) && 
                ( mov.end >= last_move_played.end ) ) ) {
                    returned.push_back( mov );
                }
            }
            return returned;
        case 40:
        case 4:// line RIGHT
            all_moves = AllPossibleMoves( b , list_pieces );
            for ( Move mov : all_moves ) {
                if ( b->B[ mov.beg ].piece == KING ||
                ( ( mov.end/8 == king.position/8 ) && 
                ( mov.end > king.position ) && 
                ( mov.end <= last_move_played.end ) ) ) {
                    returned.push_back( mov );
                }
            }
            return returned;
        case 50:
        case 5:// diagonal UP LEFT
            all_moves = AllPossibleMoves( b , list_pieces );
            for ( Move mov : all_moves ) {
                if ( b->B[ mov.beg ].piece == KING ||
                ( ( mov.end/8 + mov.end%8 == king.position/8 + king.position%8 ) && 
                ( mov.end > king.position ) && 
                ( mov.end <= last_move_played.end ) ) ) {
                    returned.push_back( mov );
                }
            }
            return returned;
        case 60:
        case 6:// diagonal DOWN RIGHT 
            all_moves = AllPossibleMoves( b , list_pieces );
            for ( Move mov : all_moves ) {
                if ( b->B[ mov.beg ].piece == KING ||
                ( ( mov.end/8 + mov.end%8 == king.position/8 + king.position%8 ) && 
                ( mov.end < king.position ) && 
                ( mov.end >= last_move_played.end ) ) ) {
                    returned.push_back( mov );
                }
            }
            return returned;
        case 70:
        case 7:// diagonal UP RIGHT 
            all_moves = AllPossibleMoves( b , list_pieces );
            for ( Move mov : all_moves ) {
                if ( b->B[ mov.beg ].piece == KING ||
                ( ( mov.end/8 + 8 - mov.end%8 == king.position/8 + 8 - king.position%8 ) && 
                ( mov.end > king.position ) && 
                ( mov.end <= last_move_played.end ) ) ) {
                    returned.push_back( mov );
                }
            }
            return returned;
        case 80:
        case 8:// diagonal DOWN RIGHT 
            all_moves = AllPossibleMoves( b , list_pieces );
            for ( Move mov : all_moves ) {
                if ( b->B[ mov.beg ].piece == KING ||
                ( ( mov.end/8 + 8 - mov.end%8 == king.position/8 + 8 - king.position%8 ) && 
                ( mov.end < king.position ) && 
                ( mov.end >= last_move_played.end ) ) ) {
                    returned.push_back( mov );
                }
            }
            return returned;


        case 100:// you have to take the knight or the king has to move
            all_moves = AllPossibleMoves( b , list_pieces );
            for ( Move mov : all_moves ) {
                if (  b->B[ mov.beg ].piece == KING || 
                mov.end == last_move_played.end ) {
                    returned.push_back( mov );
                }
            }
            return returned;


        default:// that means that the king is checked from 2 distinct directions
// no piece can protect him from two sides, so he has to move
	        Move added_move;
			added_move.beg = king.position;
            Board temporary = *b;
            Piece took;
            if ( king.position%8 > 0 ) {
                if ( b->B[ king.position - 1 ].piece == EMPTY || b->B[ king.position - 1 ].colour != b->B[ king.position].colour ) {
                    added_move.end =  (square) ( king.position - 1 );
                    took = temporary.B[ added_move.end ];
                    UpdateBoard( &temporary , added_move );
                    if ( ValidKingMove( temporary , added_move ) ) {
                        returned.push_back( added_move );
                    }
                    temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                    temporary.B[ added_move.end ] = took;
                }
                if ( king.position < 56 ) {
                    if ( b->B[ king.position + 7 ].piece == EMPTY || b->B[ king.position + 7 ].colour != b->B[ king.position].colour ) {
                        added_move.end =  (square) ( king.position + 7 );
                        took = temporary.B[ added_move.end ];
                        UpdateBoard( &temporary , added_move );
                        if ( ValidKingMove( temporary , added_move ) ) {
                            returned.push_back( added_move );
                        }
                        temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                        temporary.B[ added_move.end ] = took;
                    }
                    if ( b->B[ king.position + 8 ].piece == EMPTY || b->B[ king.position + 8 ].colour != b->B[ king.position].colour ) {
                        added_move.end =  (square) ( king.position + 8 );
                        took = temporary.B[ added_move.end ];
                        UpdateBoard( &temporary , added_move );
                        if ( ValidKingMove( temporary , added_move ) ) {
                            returned.push_back( added_move );
                        }
                        temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                        temporary.B[ added_move.end ] = took;
                    }
                    if ( king.position > 7 ) {
                        if ( b->B[ king.position - 8 ].piece == EMPTY || b->B[ king.position - 8 ].colour != b->B[ king.position].colour ) {
                            added_move.end =  (square) ( king.position - 8 );
                            took = temporary.B[ added_move.end ];
                            UpdateBoard( &temporary , added_move );
                            if ( ValidKingMove( temporary , added_move ) ) {
                                returned.push_back( added_move );
                            }
                            temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                            temporary.B[ added_move.end ] = took;
                        }
                        if ( b->B[ king.position - 9 ].piece == EMPTY || b->B[ king.position - 9 ].colour != b->B[ king.position].colour ) {
                            added_move.end =  (square) ( king.position - 9 );
                            took = temporary.B[ added_move.end ];
                            UpdateBoard( &temporary , added_move );
                            if ( ValidKingMove( temporary , added_move ) ) {
                                returned.push_back( added_move );
                            }
                            temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                            temporary.B[ added_move.end ] = took;
                        }
                        if ( king.position%8 < 7 ) {
                            if ( b->B[ king.position - 7 ].piece == EMPTY || b->B[ king.position - 7 ].colour != b->B[ king.position].colour ) {
                                added_move.end =  (square) ( king.position - 7 );
                                took = temporary.B[ added_move.end ];
                                UpdateBoard( &temporary , added_move );
                                if ( ValidKingMove( temporary , added_move ) ) {
                                    returned.push_back( added_move );
                                }
                                temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                                temporary.B[ added_move.end ] = took;
                            }
                            if ( b->B[ king.position + 1 ].piece == EMPTY || b->B[ king.position + 1 ].colour != b->B[ king.position].colour ) {
                                added_move.end =  (square) ( king.position + 1 );
                                took = temporary.B[ added_move.end ];
                                UpdateBoard( &temporary , added_move );
                                if ( ValidKingMove( temporary , added_move ) ) {
                                    returned.push_back( added_move );
                                }
                                temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                                temporary.B[ added_move.end ] = took;
                            }
                            if ( b->B[ king.position + 9 ].piece == EMPTY || b->B[ king.position + 9 ].colour != b->B[ king.position].colour ) {
                                added_move.end =  (square) ( king.position + 9 );
                                took = temporary.B[ added_move.end ];
                                UpdateBoard( &temporary , added_move );
                                if ( ValidKingMove( temporary , added_move ) ) {
                                    returned.push_back( added_move );
                                }
                                temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                                temporary.B[ added_move.end ] = took;
                            }
                        }
                    } else {
                        if ( king.position%8 < 7 ) {
                            if ( b->B[ king.position + 1 ].piece == EMPTY || b->B[ king.position + 1 ].colour != b->B[ king.position].colour ) {
                                added_move.end =  (square) ( king.position + 1 );
                                took = temporary.B[ added_move.end ];
                                UpdateBoard( &temporary , added_move );
                                if ( ValidKingMove( temporary , added_move ) ) {
                                    returned.push_back( added_move );
                                }
                                temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                                temporary.B[ added_move.end ] = took;
                            }
                            if ( b->B[ king.position + 9 ].piece == EMPTY || b->B[ king.position + 9 ].colour != b->B[ king.position].colour ) {
                                added_move.end =  (square) ( king.position + 9 );
                                took = temporary.B[ added_move.end ];
                                UpdateBoard( &temporary , added_move );
                                if ( ValidKingMove( temporary , added_move ) ) {
                                    returned.push_back( added_move );
                                }
                                temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                                temporary.B[ added_move.end ] = took;
                            }
                        }
                    }
                } else {
                    if ( b->B[ king.position - 8 ].piece == EMPTY || b->B[ king.position - 8 ].colour != b->B[ king.position].colour ) {
                        added_move.end =  (square) ( king.position - 8 );
                        took = temporary.B[ added_move.end ];
                        UpdateBoard( &temporary , added_move );
                        if ( ValidKingMove( temporary , added_move ) ) {
                            returned.push_back( added_move );
                        }
                        temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                        temporary.B[ added_move.end ] = took;
                    }
                    if ( b->B[ king.position - 9 ].piece == EMPTY || b->B[ king.position - 9 ].colour != b->B[ king.position].colour ) {
                        added_move.end =  (square) ( king.position - 9 );
                        took = temporary.B[ added_move.end ];
                        UpdateBoard( &temporary , added_move );
                        if ( ValidKingMove( temporary , added_move ) ) {
                            returned.push_back( added_move );
                        }
                        temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                        temporary.B[ added_move.end ] = took;
                    }
                    if ( king.position%8 < 7 ) {
                        if ( b->B[ king.position - 7 ].piece == EMPTY || b->B[ king.position - 7 ].colour != b->B[ king.position].colour ) {
                            added_move.end =  (square) ( king.position - 7 );
                            took = temporary.B[ added_move.end ];
                            UpdateBoard( &temporary , added_move );
                            if ( ValidKingMove( temporary , added_move ) ) {
                                returned.push_back( added_move );
                            }
                            temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                            temporary.B[ added_move.end ] = took;
                        }
                        if ( b->B[ king.position + 1 ].piece == EMPTY || b->B[ king.position + 1 ].colour != b->B[ king.position].colour ) {
                            added_move.end =  (square) ( king.position + 1 );
                            took = temporary.B[ added_move.end ];
                            UpdateBoard( &temporary , added_move );
                            if ( ValidKingMove( temporary , added_move ) ) {
                                returned.push_back( added_move );
                            }
                            temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                            temporary.B[ added_move.end ] = took;
                        }
                    }
                }
            } else {
                if ( b->B[ king.position + 1 ].piece == EMPTY || b->B[ king.position + 1 ].colour != b->B[ king.position].colour ) {
                    added_move.end =  (square) ( king.position + 1 );
                    took = temporary.B[ added_move.end ];
                    UpdateBoard( &temporary , added_move );
                    if ( ValidKingMove( temporary , added_move ) ) {
                        returned.push_back( added_move );
                    }
                    temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                    temporary.B[ added_move.end ] = took;
                }
                if ( king.position < 56 ) {
                    if ( b->B[ king.position + 8 ].piece == EMPTY || b->B[ king.position + 8 ].colour != b->B[ king.position].colour ) {
                        added_move.end =  (square) ( king.position + 8 );
                        took = temporary.B[ added_move.end ];
                        UpdateBoard( &temporary , added_move );
                        if ( ValidKingMove( temporary , added_move ) ) {
                            returned.push_back( added_move );
                        }
                        temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                        temporary.B[ added_move.end ] = took;
                    }
                    if ( b->B[ king.position + 9 ].piece == EMPTY || b->B[ king.position + 9 ].colour != b->B[ king.position].colour ) {
                        added_move.end =  (square) ( king.position + 9 );
                        took = temporary.B[ added_move.end ];
                        UpdateBoard( &temporary , added_move );
                        if ( ValidKingMove( temporary , added_move ) ) {
                            returned.push_back( added_move );
                        }
                        temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                        temporary.B[ added_move.end ] = took;
                    }
                    
                    if ( king.position > 7 ) {
                        if ( b->B[ king.position - 8 ].piece == EMPTY || b->B[ king.position - 8 ].colour != b->B[ king.position].colour ) {
                            added_move.end =  (square) ( king.position - 8 );
                            took = temporary.B[ added_move.end ];
                            UpdateBoard( &temporary , added_move );
                            if ( ValidKingMove( temporary , added_move ) ) {
                                returned.push_back( added_move );
                            }
                            temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                            temporary.B[ added_move.end ] = took;
                        }
                        if ( b->B[ king.position - 7 ].piece == EMPTY || b->B[ king.position - 7 ].colour != b->B[ king.position].colour ) {
                            added_move.end =  (square) ( king.position - 7 );
                            took = temporary.B[ added_move.end ];
                            UpdateBoard( &temporary , added_move );
                            if ( ValidKingMove( temporary , added_move ) ) {
                                returned.push_back( added_move );
                            }
                            temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                            temporary.B[ added_move.end ] = took;
                        }
                    }
                } else {
                    if ( b->B[ king.position - 8 ].piece == EMPTY || b->B[ king.position - 8 ].colour != b->B[ king.position].colour ) {
                        added_move.end =  (square) ( king.position - 8 );
                        took = temporary.B[ added_move.end ];
                        UpdateBoard( &temporary , added_move );
                        if ( ValidKingMove( temporary , added_move ) ) {
                            returned.push_back( added_move );
                        }
                        temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                        temporary.B[ added_move.end ] = took;
                    }
                    if ( b->B[ king.position - 7 ].piece == EMPTY || b->B[ king.position - 7 ].colour != b->B[ king.position].colour ) {
                        added_move.end =  (square) ( king.position - 7 );
                        took = temporary.B[ added_move.end ];
                        UpdateBoard( &temporary , added_move );
                        if ( ValidKingMove( temporary , added_move ) ) {
                            returned.push_back( added_move );
                        }
                        temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
                        temporary.B[ added_move.end ] = took;
                    }
                }
            }
            return returned;
            break;
    }


}