#include "chiass_v1.hh"

ValuedMove chIAssMove( Board* b, fast8 current_depth, fast8 expected_depth, 
std::vector<PieceAndPosition> your_pieces, std::vector<PieceAndPosition> opponent_pieces, 
Move last_move_played ) {
/*    if ( your_pieces.size() == 0 ) {
        printf("No pieces, aborting this sequence\n");
    }
    if ( your_pieces[0].piece < KING ) {// King has been taken, game is lost
// TODO probleme here -> should never happen
        return {{ (square) 0 , (square) 0  }, 0 };
    }*/
    std::vector<Move> moves;
    ufast8 result_check = CheckCheck( b, last_move_played, your_pieces[0] );
    if ( result_check == 0 ) {// the king isn't in check
        moves = AllPossibleMoves( b, your_pieces );
    } else {
        moves = AllPossibleMovesCheck( b, your_pieces, last_move_played, your_pieces[0], result_check );
    }
    if ( moves.size() == 0 ) {// No possibility : this game is lost
        if ( result_check == 0 ) {// PAT
            if ( your_pieces[0].colour == WHITE ) {// that means black has pated, so it needs the lowest value
                return {{ (square) 0 , (square) 0  }, -5000+current_depth };
            } else {
                return {{ (square) 0 , (square) 0  }, 5000-current_depth };
            }
        } else {
            if ( your_pieces[0].colour == WHITE ) {// that means black has mated, so it needs the lowest value
                return {{ (square) 0 , (square) 0  }, -10000+current_depth };
            } else {
                return {{ (square) 0 , (square) 0  }, 10000-current_depth };
            }
        }
    }
/*    if ( last_move_played == (Move){c7,b6} ) {
        printf("supposed mat number of moves : %d == 0\n\t%d %d == 50 41 == c7 b6 \n", moves.size(), moves[0].beg, moves[0].end );
        printf("\t%d %d\n", last_move_played.beg, last_move_played.end);
        sleep(5);
    }*/
    ValuedMove best_move;
    std::vector<ValuedMove> moves_values;
// The piece that's been taken, if ever, or {empty, WHITE}, so we can actually unplay the move
    PieceAndPosition played;
// the piece that is played
// necessary if a pawn is promoted
    Piece moved;

    if ( current_depth == expected_depth ) {
        return {last_move_played, BoardValue(b, your_pieces, opponent_pieces)};
    } else {
        moves_values.reserve( moves.size() );
        for ( ufast8 i = 0 ; i < moves.size() ; i++ ) {
// play the move
//TODO
            if ( moves[i].beg < 64 ) {// else, it's a castle
                moved = b->B[ moves[i].beg ];
            }
            played = Play( b, moves[i] );
// TODO : castle
// You can't remember the position of the piece that's moving, since it can change during the recursive call
// updating the piece position in the vector
            for ( ufast8 j = 0 ; j < your_pieces.size() ; j++ ) {
                if ( moves[i].beg == your_pieces[j].position ) {
                    your_pieces[j].position = moves[i].end;
                    break;
                }
            }
// if an opponent piece has been taken
            if ( played.piece != EMPTY ) {
                for ( ufast8 j = 0 ; j < opponent_pieces.size() ; j++ ) {
                    if ( played.position == opponent_pieces[j].position ) {
                        opponent_pieces[j] = opponent_pieces.back();
                        opponent_pieces.pop_back();
                        break;
                    }
                }
            }
// Value the position
            moves_values.push_back( chIAssMove( b, current_depth + 1 , expected_depth , 
            opponent_pieces, your_pieces,
            moves[i] ) );
    

// unplay the move

            if ( played.piece != EMPTY ) {
                opponent_pieces.push_back( played );
            }
            switch ( moves[i].beg ) {
                case 101:// WHITE LEFT 
                    b->B[0] = { ROOK, WHITE };
                    b->B[4] = { KING, WHITE };
                    b->B[2] = { EMPTY, WHITE };
                    b->B[3] = { EMPTY, WHITE };
                    break;
                case 102:// WHITE RIGHT 
                    b->B[4] = { KING_CASTLED, WHITE };
                    b->B[7] = { ROOK, WHITE };
                    b->B[6] = { EMPTY, WHITE };
                    b->B[5] = { EMPTY, WHITE };
                    break;
                case 103:// BLACK LEFT 
                    b->B[60] = { KING, BLACK };
                    b->B[56] = { ROOK, BLACK };
                    b->B[58] = { EMPTY, WHITE };
                    b->B[59] = { EMPTY, WHITE };
                    break;
                case 104:// BLACK RIGHT 
                    b->B[60] = { KING, BLACK };
                    b->B[63] = { ROOK, BLACK };
                    b->B[62] = { EMPTY, WHITE };
                    b->B[61] = { EMPTY, WHITE };
                    break;
                default:
// put the piece back in its place, in the vector of pieces
                    for ( ufast8 j = 0 ; j < your_pieces.size() ; j++ ) {
                        if ( moves[i].end == your_pieces[j].position ) {
                            your_pieces[j].position = moves[i].beg;
                            break;
                        }
                    }
// puts back the taken piece
                    b->B[ played.position ].piece = played.piece;
                    b->B[ played.position ].colour = (colour) played.colour;
                    b->B[ moves[i].beg ] = moved;
            }
        }
// pick the highest value
// TODO Blas?
        best_move = (ValuedMove) {moves[0] , moves_values[0].value};
        switch ( your_pieces[0].colour ) {
            case WHITE:
                for ( ufast8 i = 0 ; i < moves_values.size() ; i++ ) {
                    if ( moves_values[i].value > best_move.value ) {
                        best_move = (ValuedMove) {moves[i] , moves_values[i].value};
                    }
                }
                break;
            case BLACK:
                for ( ufast8 i = 0 ; i < moves_values.size() ; i++ ) {
                    if ( moves_values[i].value < best_move.value ) {
                        best_move = (ValuedMove) {moves[i] , moves_values[i].value};
                    }
                }
        }
// clear memory -> automatic since no pointers

    }




/*if ( current_depth == 2 && last_move_played == (Move){c6,a5} && b->B[f7].piece == KNIGHT) {
    printf("All moves at critical point, and their values : \n");
    for ( ufast8 i = 0 ; i < moves.size() ; i++ ) {
        printf("value : %ld, move : ", moves_values[i].value);
        std::cout << moves[i];
        std::cout<<" followed by "<< moves_values[i]<<"\n";
    }
    printf("color : %d, best value : %ld, move : %d %d\n",your_pieces[0].colour==WHITE,best_move.value, best_move.m.beg, best_move.m.end);
}*/


/*
if ( current_depth == 0 ) {
    printf("All moves at depth 0, and their values : \n");
    for ( ufast8 i = 0 ; i < moves.size() ; i++ ) {
        printf("value : %ld, move : ", moves_values[i].value);
        std::cout << moves[i];
        std::cout<<" followed by "<< moves_values[i]<<"\n";
    }
    printf("color : %d, best value : %ld, move : %d %d\n",your_pieces[0].colour==WHITE,best_move.value, 
    best_move.m.beg, best_move.m.end);
}
*/
// return it, and the associated move
    return best_move;
}