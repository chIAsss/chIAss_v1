#include "chiass_v1.hh"

Move chIAssWaitForOpponent(void) {
    fprintf(stdout , "Waiting for input:\nBeginning case : ");
    std::string beg;
    std::string  end;
    
    std::cin >> beg;
    fprintf(stdout, "Ending case : ");
    std::cin >> end;
    
    Move returned;

    if ( end.size() > 2 ) {
        if ( end == "clw" ) {
            returned.end = (square)101;
        } else if ( end == "clb" ) {
            returned.end = (square)103;
        } else if ( end == "crw" ) {
            returned.end = (square)102;
        } else if ( end == "crb" ) {
            returned.end = (square)104;
        } else {// Promotion
            returned.beg = (square) ( (int) beg[0] -97 + ( (int) beg[1] - 49 ) * 8 );
            returned.end = (square) ( (int) end[2] -97 + ( (int) end[3] - 49 ) * 8 );
            if ( returned.end < 8 ) {
                returned.end = (square) (returned.end + 56) ;
            }
            if ( end[1] == 'q' ) {
                returned.end = (square) (returned.end + 8);
            } else if ( end[1] == 'k' ) {
                returned.end = (square) (returned.end + 16);
            } else if ( end[1] == 'r' ) {
                returned.end = (square) (returned.end + 24);
            } else if ( end[1] == 'b' ) {
                returned.end = (square) (returned.end + 32);
            }
        }
        return returned;
    }
    returned.beg = (square) ( (int) beg[0] -97 + ( (int) beg[1] - 49 ) * 8 );
    returned.end = (square) ( (int) end[0] -97 + ( (int) end[1] - 49 ) * 8 );
    return returned;
} 