#include "chiass_v1.hh"

/*std::vector<Move> Moves( const Board b , const std::vector<PieceAndPosition> list_pieces ,
Move last_move_played, PieceAndPosition king ) {
	u8 result = CheckCheck( b , last_move_played , king );
	if ( result  == 0 ) {
		return AllPossibleMoves( b , list_pieces );
	}
	return AllPossibleMovesCheck( b, list_pieces, Move last_move_played, PieceAndPosition king, result );
}*/



// Always start with up and bottom, to evade as much %8 as possible
// Change Queen moves :  add a jump to bishop ?
std::vector<Move> AllPossibleMoves( const Board* b , const std::vector<PieceAndPosition> list_pieces ) {
	// CheckCheck
// TODO reserve
	Board temporary = *b;
	std::vector<Move> returned;
	Move added_move;
	Piece took;
	u8 checking_square;
	u8 is_pinned;
	for ( PieceAndPosition p : list_pieces ) {
		is_pinned = IsPinnedDown( b , p );
		switch ( is_pinned ) {
			case 0:// the piece is pinned down
				continue;

			case 1:
			added_move.beg = p.position;
			switch ( p.piece ) {
				case PAWN:
					switch ( p.colour ) {
						case WHITE:
							if ( b->B[ p.position + 8 ].piece == EMPTY ) {
								if ( added_move.beg/8 != 6 ) {
									added_move.end =  (square) ( p.position + 8 );
								} else {
									added_move.end = (square) ( p.position + 16 );
								}
								returned.push_back( added_move );
							}
							if ( p.position %8 > 0 ) {
								if ( b->B[ p.position + 7 ].piece != EMPTY ) {
									if ( b->B[ p.position + 7 ].colour == BLACK ) {
										if ( added_move.beg/8 != 6 ) {
											added_move.end =  (square) ( p.position + 7 );
										} else {
											added_move.end = (square) ( p.position + 15 );
										}
										returned.push_back( added_move );
									}
								}
								if ( p.position %8 < 7 ) {
									if ( b->B[ p.position + 9 ].piece != EMPTY ) {
										if ( b->B[ p.position + 9 ].colour == BLACK ) {
											if ( added_move.beg/8 != 6 ) {
												added_move.end =  (square) ( p.position + 9 );
											} else {
												added_move.end = (square) ( p.position + 17 );
											}
											returned.push_back( added_move );
										}
									}
								}
							} else {// Still on my shennigans to evade as much tests as possible, if position%8 == 0, we don't test for < 7
								if ( b->B[ p.position + 9 ].piece != EMPTY ) {
									if ( b->B[ p.position + 9 ].colour == BLACK ) {
										if ( added_move.beg/8 != 6 ) {
											added_move.end =  (square) ( p.position + 9 );
										} else {
											added_move.end = (square) ( p.position + 17 );
										}
										returned.push_back( added_move );
									}
								}
							}
							break;
						case BLACK:
							if ( b->B[ p.position - 8 ].piece == EMPTY ) {
								if ( added_move.beg/8 != 1 ) {
									added_move.end =  (square) ( p.position - 8 );
								} else {
									added_move.end = (square) ( p.position + 56 );
								}
								returned.push_back( added_move );
							}
							if ( p.position %8 < 7 ) {
								if ( b->B[ p.position - 7 ].piece != EMPTY ) {
									if ( b->B[ p.position - 7 ].colour == WHITE ) {
										if ( added_move.beg/8 != 1 ) {
											added_move.end =  (square) ( p.position - 7 );
										} else {
											added_move.end = (square) ( p.position + 57 );
										}
										returned.push_back( added_move );
									}
								}
								if ( p.position%8 > 0) {
									if ( b->B[ p.position - 9 ].piece != EMPTY ) {
										if ( b->B[ p.position - 9 ].colour == WHITE ) {
											if ( added_move.beg/8 != 1 ) {
												added_move.end =  (square) ( p.position - 9 );
											} else {
												added_move.end = (square) ( p.position + 55 );
											}
											returned.push_back( added_move );
										}
									}
								}
							} else {// Still on my shennigans to evade as much tests as possible, if position%8 == 8, we don't test for > 0
								if ( b->B[ p.position - 9 ].piece != EMPTY ) {
									if ( b->B[ p.position - 9 ].colour == WHITE ) {
										if ( added_move.beg/8 != 1 ) {
											added_move.end =  (square) ( p.position - 9 );
										} else {
											added_move.end = (square) ( p.position + 55 );
										}
										returned.push_back( added_move );
									}
								}
							}
							break;
					}
					break;
				case KNIGHT:
	// TODO: is != faster than <
	// TODO : compact the tests -> could the order be optimized?
	// TODO rewritten tests, maybe?
	// Note : we always end by the % comparison, since it's more costly than the basic one
	// This might be improved by analyzing games and figuring out which tests are the most common, or maybe even enlarge the board
	/*// first square
	added_move.end =  (square) ( p.position + 6 );
	returned.push_back( added_move );
	// second square 
	added_move.end =  (square) ( p.position + 1 )5;
	returned.push_back( added_move );
	// third square 
	added_move.end =  (square) ( p.position + 1 )7;
	returned.push_back( added_move );
	// fourth square 
	added_move.end =  (square) ( p.position + 1 )0;
	returned.push_back( added_move );
	// fifth square 
	added_move.end =  (square) ( p.position - 6 );
	returned.push_back( added_move );
	// sixth square 
	added_move.end =  (square) ( p.position - 1 )5;
	returned.push_back( added_move );
	// seventh square 
	added_move.end =  (square) ( p.position - 1 )7;
	returned.push_back( added_move );
	// eighth square 
	added_move.end =  (square) ( p.position - 1 )0;
	returned.push_back( added_move ); */

	// 4 up squares
					if ( p.position < 48 ) {
						if ( p.position%8 > 1 ) {
							if ( b->B[ p.position + 6 ].piece == EMPTY  ||  b->B[ p.position + 6 ].colour != p.colour ) {
	// first square
								added_move.end =  (square) ( p.position + 6 );
								returned.push_back( added_move );
							}
							if ( b->B[ p.position + 15 ].piece == EMPTY  ||  b->B[ p.position + 15 ].colour != p.colour ) {
	// second square 
								added_move.end =  (square) ( p.position + 15);
								returned.push_back( added_move );
							}

							if ( p.position%8 < 6 ) {
								if ( b->B[ p.position + 17 ].piece == EMPTY  ||  b->B[ p.position + 17 ].colour != p.colour ) {
	// third square
									added_move.end =  (square) ( p.position + 17 );
									returned.push_back( added_move );
								}
								if ( b->B[ p.position + 10 ].piece == EMPTY  ||  b->B[ p.position + 10 ].colour != p.colour ) {
	// fourth square 
									added_move.end =  (square) ( p.position + 10);
									returned.push_back( added_move );
								}
							} else if ( p.position%8 == 6 ) {
								if ( b->B[ p.position + 17 ].piece == EMPTY  ||  b->B[ p.position + 17 ].colour != p.colour ) {
	// third square
									added_move.end =  (square) ( p.position + 17);
									returned.push_back( added_move );
								}
							}

						} else {// p.position%8 <= 1
							if ( p.position%8 == 1 ) { //  if not > 1, then > 0 is equivalent to == 1
								if ( b->B[ p.position + 15 ].piece == EMPTY  ||  b->B[ p.position + 15 ].colour != p.colour ) {
		// second square 
									added_move.end =  (square) ( p.position + 15);
									returned.push_back( added_move );
								}
							}
							// p.position%8 <= 1 implies that p.position%8 < 6
							if ( b->B[ p.position + 17 ].piece == EMPTY  ||  b->B[ p.position + 17 ].colour != p.colour ) {
	// third square
								added_move.end =  (square) ( p.position + 17);
								returned.push_back( added_move );
							}
							if ( b->B[ p.position + 10 ].piece == EMPTY  ||  b->B[ p.position + 10 ].colour != p.colour ) {
	// fourth square 
								added_move.end =  (square) ( p.position + 10);
								returned.push_back( added_move );
							}
						}



						if ( p.position > 15 ) {
							if ( p.position%8 > 1 ) {
								if ( p.position%8 < 6 ) {
									if ( b->B[ p.position - 6 ].piece == EMPTY  ||  b->B[ p.position - 6 ].colour != p.colour ) {
	// fifth square 
										added_move.end =  (square) ( p.position - 6 );
										returned.push_back( added_move );
									}
									if ( b->B[ p.position - 15 ].piece == EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
	// sixth square 
										added_move.end =  (square) ( p.position - 15);
										returned.push_back( added_move );
									}
								} else if ( p.position%8 == 6 ) {// if not > 1, then < 7 is equivalent to == 6
									if ( b->B[ p.position - 15 ].piece == EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
	// sixth square 
										added_move.end =  (square) ( p.position - 15);
										returned.push_back( added_move );
									}
								}
								if ( b->B[ p.position - 17 ].piece == EMPTY  ||  b->B[ p.position - 17 ].colour != p.colour ) {
	// seventh square
									added_move.end =  (square) ( p.position - 17);
									returned.push_back( added_move );
								}
								if ( b->B[ p.position - 10 ].piece == EMPTY  ||  b->B[ p.position - 10 ].colour != p.colour ) {
	// eighth square 
									added_move.end =  (square) ( p.position - 10);
									returned.push_back( added_move );
								}

							} else {
								// p.position%8 <= 1 implies p.position%8 < 6
								if ( b->B[ p.position - 6 ].piece == EMPTY  ||  b->B[ p.position - 6 ].colour != p.colour ) {
	// fifth square 
									added_move.end =  (square) ( p.position - 6 );
									returned.push_back( added_move );
								}
								if ( b->B[ p.position - 15 ].piece == EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
	// sixth square 
									added_move.end =  (square) ( p.position - 15);
									returned.push_back( added_move );
								}
								if ( p.position%8 == 1 ) {// if not > 1, then > 0 is equivalent to == 1
									if ( b->B[ p.position - 17 ].piece == EMPTY  ||  b->B[ p.position - 17 ].colour != p.colour ) {
	// seventh square 
										added_move.end =  (square) ( p.position - 17);
										returned.push_back( added_move );
									
									}
								}
							}


						} else if ( p.position > 7 ) { // only the 5th and 8th moves are possible
							if ( p.position%8 < 6 ) {
								if ( b->B[ p.position - 6 ].piece == EMPTY  ||  b->B[ p.position - 6 ].colour != p.colour ) {
	// fifth square 
									added_move.end =  (square) ( p.position - 6 );
									returned.push_back( added_move );
								}
								if ( p.position%8 > 1 ) {
									if ( b->B[ p.position - 10 ].piece == EMPTY  ||  b->B[ p.position - 10 ].colour != p.colour ) {
	// eighth square 
										added_move.end =  (square) ( p.position - 10);
										returned.push_back( added_move );
									}
								}
							} else {// p.position%8 >= 6, then it's also > 1 , the 8th move is necessarily an option
								if ( b->B[ p.position - 10 ].piece == EMPTY  ||  b->B[ p.position - 10 ].colour != p.colour ) {
	// eighth square 
									added_move.end =  (square) ( p.position - 10);
									returned.push_back( added_move );
								}
							}
						}





					} else { // position >= 48, so it's > 15, only have to check for 5th through 8th moves
						if ( p.position < 56 ) {
							if ( p.position > 49 ) {
								if ( b->B[ p.position + 6 ].piece == EMPTY  ||  b->B[ p.position + 6 ].colour != p.colour ) {
// first square
									added_move.end =  (square) ( p.position + 6 );
									returned.push_back( added_move );
								}
								if ( p.position < 54 ) {
									if ( b->B[ p.position + 10 ].piece == EMPTY  ||  b->B[ p.position + 10 ].colour != p.colour ) {
// fourth square 
										added_move.end =  (square) ( p.position + 10);
										returned.push_back( added_move );
									}
									if ( b->B[ p.position - 6 ].piece == EMPTY  ||  b->B[ p.position - 6 ].colour != p.colour ) {
// fifth square 
										added_move.end =  (square) ( p.position - 6 );
										returned.push_back( added_move );
									}
									if ( b->B[ p.position - 15 ].piece == EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
// sixth square 
										added_move.end =  (square) ( p.position - 15);
										returned.push_back( added_move );
									}
								} else if ( p.position == 54 ) {// if not > 1, then < 7 is equivalent to == 6
									if ( b->B[ p.position - 15 ].piece == EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
// sixth square 
										added_move.end =  (square) ( p.position - 15);
										returned.push_back( added_move );
									}
								}
								if ( b->B[ p.position - 17 ].piece == EMPTY  ||  b->B[ p.position - 17 ].colour != p.colour ) {
// seventh square
									added_move.end =  (square) ( p.position - 17);
									returned.push_back( added_move );
								}
								if ( b->B[ p.position - 10 ].piece == EMPTY  ||  b->B[ p.position - 10 ].colour != p.colour ) {
// eighth square 
									added_move.end =  (square) ( p.position - 10);
									returned.push_back( added_move );
								}
							} else {// p.position < 50 && p.position >= 48
								if ( b->B[ p.position + 10 ].piece == EMPTY  ||  b->B[ p.position + 10 ].colour != p.colour ) {
// fourth square 
									added_move.end =  (square) ( p.position + 10);
									returned.push_back( added_move );
								}
								if ( b->B[ p.position - 6 ].piece == EMPTY  ||  b->B[ p.position - 6 ].colour != p.colour ) {
// fifth square 
									added_move.end =  (square) ( p.position - 6 );
									returned.push_back( added_move );
								}
								if ( b->B[ p.position - 15 ].piece == EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
// sixth square 
									added_move.end =  (square) ( p.position - 15);
									returned.push_back( added_move );
								}
								if ( p.position == 49 ) {
									if ( b->B[ p.position - 17 ].piece == EMPTY  ||  b->B[ p.position - 17 ].colour != p.colour ) {
// seventh square
										added_move.end =  (square) ( p.position - 17);
										returned.push_back( added_move );
									}
								}
							}
						} else {// p.position >= 56 , 5 through 8
							if ( p.position < 62 ) {
								if ( b->B[ p.position - 6 ].piece == EMPTY  ||  b->B[ p.position - 6 ].colour != p.colour ) {
// fifth square 
									added_move.end =  (square) ( p.position - 6 );
									returned.push_back( added_move );
								}
								if ( b->B[ p.position - 15 ].piece == EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
// sixth square 
									added_move.end =  (square) ( p.position - 15);
									returned.push_back( added_move );
								}
							} else if ( p.position == 62 ) {// if not > 1, then < 7 is equivalent to == 6
								if ( b->B[ p.position - 15 ].piece == EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
// sixth square 
									added_move.end =  (square) ( p.position - 15);
									returned.push_back( added_move );
								}
							}
							if ( p.position > 57 ) {
								if ( b->B[ p.position - 17 ].piece == EMPTY  ||  b->B[ p.position - 17 ].colour != p.colour ) {
// seventh square
									added_move.end =  (square) ( p.position - 17);
									returned.push_back( added_move );
								}
								if ( b->B[ p.position - 10 ].piece == EMPTY  ||  b->B[ p.position - 10 ].colour != p.colour ) {
// eighth square 
									added_move.end =  (square) ( p.position - 10);
									returned.push_back( added_move );
								}
							} else if ( p.position > 57 ) {
								if ( b->B[ p.position - 17 ].piece == EMPTY  ||  b->B[ p.position - 17 ].colour != p.colour ) {
// seventh square
									added_move.end =  (square) ( p.position - 17);
									returned.push_back( added_move );
								}
							}
						}
					}
					break;

				case BISHOP:
	// Up left
						checking_square = (u8) ( p.position + 7 );
	// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
						while ( checking_square < 64 && checking_square%8 != 7 ) {
							if ( b->B[ checking_square ].piece == EMPTY ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );

							} else {
								if ( b->B[ checking_square ].colour != p.colour ) {
									added_move.end = (square) checking_square;
									returned.push_back( added_move );
								}
								break;
							}
							checking_square += 7;
						}
	// Up right
						checking_square = (u8) ( p.position + 9 );
	// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
						while ( checking_square < 64 && checking_square%8 != 0 ) {
							if ( b->B[ checking_square ].piece == EMPTY ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							} else {
								if ( b->B[ checking_square ].colour != p.colour ) {
									added_move.end = (square) checking_square ;
									returned.push_back( added_move );
								}
								break;
							}
							checking_square += 9;
						}
	// Down right
						checking_square = (u8) ( p.position - 7 );
	// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
						while ( checking_square < 64 && checking_square%8 != 0 ) {
							if ( b->B[ checking_square ].piece == EMPTY ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							} else {
								if ( b->B[ checking_square ].colour != p.colour ) {
									added_move.end = (square) checking_square;
									returned.push_back( added_move );
								}
								break;
							}
							checking_square -= 7;
						}
	// Down left
						checking_square = (u8) ( p.position - 9 );
	// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
						while ( checking_square < 64 && checking_square%8 != 7 ) {
							if ( b->B[ checking_square ].piece == EMPTY ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							} else {
								if ( b->B[ checking_square ].colour != p.colour ) {
									added_move.end = (square) checking_square;
									returned.push_back( added_move );
								}
								break;
							}
							checking_square -= 9;
						}
					break;



	// The QUEEN doesn't have a break statement, so we don't have to rewrite the rook moves
				case QUEEN: 
					checking_square = (u8) ( p.position + 7 );
	// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square += 7;
					}
					checking_square = (u8) ( p.position + 9 );
	// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square += 9;
					}
					checking_square = (u8) ( p.position - 7 );
	// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square -= 7;
					}
					checking_square = (u8) ( p.position - 9 );
	// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square -= 9;
					}


				case ROOK:
					checking_square = (u8) ( p.position + 8 );
	// as long as the case is in the board : not too high
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square += 8;
					}
					checking_square = (u8) ( p.position - 8 ) ;
	// as long as the case is in the board : not too low ( which would make it appear too high, since it's unsigned, you go from 6 to 253 )
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square -= 8;
					}
					checking_square = (u8) ( p.position + 1 ) ;
	// as long as the case is in the board : not too much on the right ( which would make it pop on the left )
					while ( checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square ++;
					}
					checking_square = (u8) ( p.position - 1 );
	// as long as the case is in the board : not too much on the left ( which would make it pop on the right )
					while ( checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square --;
					}
					break;



	// TODO could be optimized..? order
				case KING:
				case KING_CASTLED_LEFT:
				case KING_CASTLED_RIGHT:
				case KING_CASTLED:
					if ( p.position%8 > 0 ) {
						if ( b->B[ p.position - 1 ].piece == EMPTY || b->B[ p.position - 1 ].colour != p.colour ) {
							added_move.end =  (square) ( p.position - 1 );
							took = temporary.B[ added_move.end ];
							UpdateBoard( &temporary , added_move );
							if ( ValidKingMove( temporary , added_move ) ) {
								returned.push_back( added_move );
							}
							temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
							temporary.B[ added_move.end ] = took;
						}
						if ( p.position < 56 ) {
							if ( b->B[ p.position + 7 ].piece == EMPTY || b->B[ p.position + 7 ].colour != p.colour ) {
								added_move.end =  (square) ( p.position + 7 );
								took = temporary.B[ added_move.end ];
								UpdateBoard( &temporary , added_move );
								if ( ValidKingMove( temporary , added_move ) ) {
									returned.push_back( added_move );
								}
								temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
								temporary.B[ added_move.end ] = took;
							}
							if ( b->B[ p.position + 8 ].piece == EMPTY || b->B[ p.position + 8 ].colour != p.colour ) {
								added_move.end =  (square) ( p.position + 8 );
								took = temporary.B[ added_move.end ];
								UpdateBoard( &temporary , added_move );
								if ( ValidKingMove( temporary , added_move ) ) {
									returned.push_back( added_move );
								}
								temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
								temporary.B[ added_move.end ] = took;
							}
							if ( p.position > 7 ) {
								if ( b->B[ p.position - 8 ].piece == EMPTY || b->B[ p.position - 8 ].colour != p.colour ) {
									added_move.end =  (square) ( p.position - 8 );
									took = temporary.B[ added_move.end ];
									UpdateBoard( &temporary , added_move );
									if ( ValidKingMove( temporary , added_move ) ) {
										returned.push_back( added_move );
									}
									temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
									temporary.B[ added_move.end ] = took;
								}
								if ( b->B[ p.position - 9 ].piece == EMPTY || b->B[ p.position - 9 ].colour != p.colour ) {
									added_move.end =  (square) ( p.position - 9 );
									took = temporary.B[ added_move.end ];
									UpdateBoard( &temporary , added_move );
									if ( ValidKingMove( temporary , added_move ) ) {
										returned.push_back( added_move );
									}
									temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
									temporary.B[ added_move.end ] = took;
								}
								if ( p.position%8 < 7 ) {
									if ( b->B[ p.position - 7 ].piece == EMPTY || b->B[ p.position - 7 ].colour != p.colour ) {
										added_move.end =  (square) ( p.position - 7 );
										took = temporary.B[ added_move.end ];
										UpdateBoard( &temporary , added_move );
										if ( ValidKingMove( temporary , added_move ) ) {
											returned.push_back( added_move );
										}
										temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
										temporary.B[ added_move.end ] = took;
									}
									if ( b->B[ p.position + 1 ].piece == EMPTY || b->B[ p.position + 1 ].colour != p.colour ) {
										added_move.end =  (square) ( p.position + 1 );
										took = temporary.B[ added_move.end ];
										UpdateBoard( &temporary , added_move );
										if ( ValidKingMove( temporary , added_move ) ) {
											returned.push_back( added_move );
										}
										temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
										temporary.B[ added_move.end ] = took;
									}
									if ( b->B[ p.position + 9 ].piece == EMPTY || b->B[ p.position + 9 ].colour != p.colour ) {
										added_move.end =  (square) ( p.position + 9 );
										took = temporary.B[ added_move.end ];
										UpdateBoard( &temporary , added_move );
										if ( ValidKingMove( temporary , added_move ) ) {
											returned.push_back( added_move );
										}
										temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
										temporary.B[ added_move.end ] = took;
									}
								}
							} else {
								if ( p.position%8 < 7 ) {
									if ( b->B[ p.position + 1 ].piece == EMPTY || b->B[ p.position + 1 ].colour != p.colour ) {
										added_move.end =  (square) ( p.position + 1 );
										took = temporary.B[ added_move.end ];
										UpdateBoard( &temporary , added_move );
										if ( ValidKingMove( temporary , added_move ) ) {
											returned.push_back( added_move );
										}
										temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
										temporary.B[ added_move.end ] = took;
									}
									if ( b->B[ p.position + 9 ].piece == EMPTY || b->B[ p.position + 9 ].colour != p.colour ) {
										added_move.end =  (square) ( p.position + 9 );
										took = temporary.B[ added_move.end ];
										UpdateBoard( &temporary , added_move );
										if ( ValidKingMove( temporary , added_move ) ) {
											returned.push_back( added_move );
										}
										temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
										temporary.B[ added_move.end ] = took;
									}
								}
							}
						} else {
							if ( b->B[ p.position - 8 ].piece == EMPTY || b->B[ p.position - 8 ].colour != p.colour ) {
								added_move.end =  (square) ( p.position - 8 );
								took = temporary.B[ added_move.end ];
								UpdateBoard( &temporary , added_move );
								if ( ValidKingMove( temporary , added_move ) ) {
									returned.push_back( added_move );
								}
								temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
								temporary.B[ added_move.end ] = took;
							}
							if ( b->B[ p.position - 9 ].piece == EMPTY || b->B[ p.position - 9 ].colour != p.colour ) {
								added_move.end =  (square) ( p.position - 9 );
								took = temporary.B[ added_move.end ];
								UpdateBoard( &temporary , added_move );
								if ( ValidKingMove( temporary , added_move ) ) {
									returned.push_back( added_move );
								}
								temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
								temporary.B[ added_move.end ] = took;
							}
							if ( p.position%8 < 7 ) {
								if ( b->B[ p.position - 7 ].piece == EMPTY || b->B[ p.position - 7 ].colour != p.colour ) {
									added_move.end =  (square) ( p.position - 7 );
									took = temporary.B[ added_move.end ];
									UpdateBoard( &temporary , added_move );
									if ( ValidKingMove( temporary , added_move ) ) {
										returned.push_back( added_move );
									}
									temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
									temporary.B[ added_move.end ] = took;
								}
								if ( b->B[ p.position + 1 ].piece == EMPTY || b->B[ p.position + 1 ].colour != p.colour ) {
									added_move.end =  (square) ( p.position + 1 );
									took = temporary.B[ added_move.end ];
									UpdateBoard( &temporary , added_move );
									if ( ValidKingMove( temporary , added_move ) ) {
										returned.push_back( added_move );
									}
									temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
									temporary.B[ added_move.end ] = took;
								}
							}
						}
					} else {
						if ( b->B[ p.position + 1 ].piece == EMPTY || b->B[ p.position + 1 ].colour != p.colour ) {
							added_move.end =  (square) ( p.position + 1 );
							took = temporary.B[ added_move.end ];
							UpdateBoard( &temporary , added_move );
							if ( ValidKingMove( temporary , added_move ) ) {
								returned.push_back( added_move );
							}
							temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
							temporary.B[ added_move.end ] = took;
						}
						if ( p.position < 56 ) {
							if ( b->B[ p.position + 8 ].piece == EMPTY || b->B[ p.position + 8 ].colour != p.colour ) {
								added_move.end =  (square) ( p.position + 8 );
								took = temporary.B[ added_move.end ];
								UpdateBoard( &temporary , added_move );
								if ( ValidKingMove( temporary , added_move ) ) {
									returned.push_back( added_move );
								}
								temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
								temporary.B[ added_move.end ] = took;
							}
							if ( b->B[ p.position + 9 ].piece == EMPTY || b->B[ p.position + 9 ].colour != p.colour ) {
								added_move.end =  (square) ( p.position + 9 );
								took = temporary.B[ added_move.end ];
								UpdateBoard( &temporary , added_move );
								if ( ValidKingMove( temporary , added_move ) ) {
									returned.push_back( added_move );
								}
								temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
								temporary.B[ added_move.end ] = took;
							}
							
							if ( p.position > 7 ) {
								if ( b->B[ p.position - 8 ].piece == EMPTY || b->B[ p.position - 8 ].colour != p.colour ) {
									added_move.end =  (square) ( p.position - 8 );
									took = temporary.B[ added_move.end ];
									UpdateBoard( &temporary , added_move );
									if ( ValidKingMove( temporary , added_move ) ) {
										returned.push_back( added_move );
									}
									temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
									temporary.B[ added_move.end ] = took;
								}
								if ( b->B[ p.position - 7 ].piece == EMPTY || b->B[ p.position - 7 ].colour != p.colour ) {
									added_move.end =  (square) ( p.position - 7 );
									took = temporary.B[ added_move.end ];
									UpdateBoard( &temporary , added_move );
									if ( ValidKingMove( temporary , added_move ) ) {
										returned.push_back( added_move );
									}
									temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
									temporary.B[ added_move.end ] = took;
								}
							}
						} else {
							if ( b->B[ p.position - 8 ].piece == EMPTY || b->B[ p.position - 8 ].colour != p.colour ) {
								added_move.end =  (square) ( p.position - 8 );
								took = temporary.B[ added_move.end ];
								UpdateBoard( &temporary , added_move );
								if ( ValidKingMove( temporary , added_move ) ) {
									returned.push_back( added_move );
								}
								temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
								temporary.B[ added_move.end ] = took;
							}
							if ( b->B[ p.position - 7 ].piece == EMPTY || b->B[ p.position - 7 ].colour != p.colour ) {
								added_move.end =  (square) ( p.position - 7 );
								took = temporary.B[ added_move.end ];
								UpdateBoard( &temporary , added_move );
								if ( ValidKingMove( temporary , added_move ) ) {
									returned.push_back( added_move );
								}
								temporary.B[ added_move.beg ] = temporary.B[ added_move.end ];
								temporary.B[ added_move.end ] = took;
							}
						}
					}
					break;
				case EMPTY:
					fprintf( stdout , "Error, EMPTY piece is in the pieces vector\n");
			}
			break;
// ================================================================
			case 2:// can only move up and down
			added_move.beg = p.position;
			switch ( p.piece ) {
				case PAWN:
					switch ( p.colour ) {
						case WHITE:
							if ( b->B[ p.position + 8 ].piece == EMPTY ) {
								added_move.end =  (square) ( p.position + 8 );
								returned.push_back( added_move );
							}
							break;
						case BLACK:
							if ( b->B[ p.position - 8 ].piece == EMPTY ) {
								added_move.end =  (square) ( p.position - 8 );
								returned.push_back( added_move );
							}
							break;
					}
					break;
				case ROOK:
				case QUEEN:
					checking_square = (u8) ( p.position + 8 );
// UP : as long as the case is in the board : not too high
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square += 8;
					}
					checking_square = (u8) ( p.position - 8 ) ;
// DOWN : as long as the case is in the board : not too low ( which would make it appear too high, since it's unsigned, you go from 6 to 253 )
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square -= 8;
					}
				default:
					continue;
				}
				break;
// ================================================================
			case 3:// can only move left and right
			added_move.beg = p.position;
// The piece is necessarily a queen or a rook
				checking_square = (u8) ( p.position + 1 ) ;
// as long as the case is in the board : not too much on the right ( which would make it pop on the left )
				while ( checking_square%8 != 0 ) {
					if ( b->B[ checking_square ].piece == EMPTY ) {
						added_move.end = (square) checking_square;
						returned.push_back( added_move );
					} else {
						if ( b->B[ checking_square ].colour != p.colour ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						}
						break;
					}
					checking_square ++;
				}
				checking_square = (u8) ( p.position - 1 );
// as long as the case is in the board : not too much on the left ( which would make it pop on the right )
				while ( checking_square%8 != 7 ) {
					if ( b->B[ checking_square ].piece == EMPTY ) {
						added_move.end = (square) checking_square;
						returned.push_back( added_move );
					} else {
						if ( b->B[ checking_square ].colour != p.colour ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						}
						break;
					}
					checking_square --;
				}
				break;
// ================================================================
			case 4:// can only move up left / down right
			added_move.beg = p.position;
			switch ( p.piece ) {
				case PAWN:
					switch ( p.colour ) {
						case WHITE:
							if ( p.position %8 > 0 ) {
								if ( b->B[ p.position + 7 ].piece != EMPTY ) {
									if ( b->B[ p.position + 7 ].colour == BLACK ) {
										added_move.end =  (square) ( p.position + 7 );
										returned.push_back( added_move );
									}
								}
							}
							break;
						case BLACK:
							if ( p.position %8 < 7 ) {
								if ( b->B[ p.position - 7 ].piece != EMPTY ) {
									if ( b->B[ p.position - 7 ].colour == WHITE ) {
										added_move.end =  (square) ( p.position - 7 );
										returned.push_back( added_move );
									}
								}
							}
							break;
					}
					break;
				case QUEEN:
				case BISHOP:
// Up left
					checking_square = (u8) ( p.position + 7 );
// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );

						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square += 7;
					}
// Down right
					checking_square = (u8) ( p.position - 7 );
// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square -= 7;
					}
				default:
					continue;
				}
				break;
// ================================================================
			case 5:// can only move up right / down left
			added_move.beg = p.position;
			switch ( p.piece ) {
				case PAWN:
					switch ( p.colour ) {
						case WHITE:
							if ( p.position %8 < 7 ) {
								if ( b->B[ p.position + 9 ].piece != EMPTY ) {
									if ( b->B[ p.position + 9 ].colour == BLACK ) {
										added_move.end =  (square) ( p.position + 9 );
										returned.push_back( added_move );
									}
								}
							}
							break;
						case BLACK:
							if ( p.position %8 > 0 ) {
								if ( b->B[ p.position - 9 ].piece != EMPTY ) {
									if ( b->B[ p.position - 9 ].colour == WHITE ) {
										added_move.end =  (square) ( p.position - 9 );
										returned.push_back( added_move );
									}
								}
							}
							break;
					}
					break;
				case QUEEN:
				case BISHOP:	
// Up right
					checking_square = (u8) ( p.position + 9 );
// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square ;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square += 9;
					}	
// Down left
					checking_square = (u8) ( p.position - 9 );
// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece == EMPTY ) {
							added_move.end = (square) checking_square;
							returned.push_back( added_move );
						} else {
							if ( b->B[ checking_square ].colour != p.colour ) {
								added_move.end = (square) checking_square;
								returned.push_back( added_move );
							}
							break;
						}
						checking_square -= 9;
					}
				default:
					continue;
			}
		}
	}
	return returned;
}
