#include "chiass_v1.hh"

fast32 BoardValue( const Board* b , std::vector<PieceAndPosition> your_pieces, std::vector<PieceAndPosition> oppo_pieces ) {
	fast32 your_sum=0;
	fast32 opponent_sum=0;
	for ( unsigned char i = 0 ; i < your_pieces.size() ; i++ ) {
		switch ( your_pieces[ i ].piece ) {
			case QUEEN:
				your_sum += 950;
				your_sum += 10*std::min( 8 - your_pieces[ i ].position%8 , your_pieces[ i ].position%8);
				your_sum += 10*std::min( 8 - your_pieces[ i ].position/8 , your_pieces[ i ].position/8);
				break;
			case ROOK:
				your_sum += 500;
				break;
			case BISHOP:
				your_sum += 330;
				your_sum += 8*std::min( 8 - your_pieces[ i ].position%8 , your_pieces[ i ].position%8);
				your_sum += 8*std::min( 8 - your_pieces[ i ].position/8 , your_pieces[ i ].position/8);
				break;
			case KNIGHT:
				your_sum += 300;
				your_sum += 7*std::min( 8 - your_pieces[ i ].position%8 , your_pieces[ i ].position%8);
				your_sum += 7*std::min( 8 - your_pieces[ i ].position/8 , your_pieces[ i ].position/8);
				break;
			case PAWN:
				your_sum += 100;
				if ( your_pieces[ i ].position%8 != 0 && b->B[ your_pieces[ i ].position + 7 ].piece != EMPTY ) {
					your_sum += 10;
				}
				if ( your_pieces[ i ].position%8 != 7 && b->B[ your_pieces[ i ].position + 9 ].piece != EMPTY ) {
					your_sum += 10;
				}
				your_sum += 5*your_pieces[ i ].position/8;
				break;
			default:
				continue;
		}
	}
	for ( unsigned char i = 0 ; i < oppo_pieces.size() ; i++ ) {
		switch ( oppo_pieces[ i ].piece ) {
			case QUEEN:
				opponent_sum -= 950;
				your_sum += 10*std::min( 8 - oppo_pieces[ i ].position%8 , oppo_pieces[ i ].position%8);
				your_sum += 10*std::min( 8 - oppo_pieces[ i ].position/8 , oppo_pieces[ i ].position/8);
				break;
			case ROOK:
				opponent_sum -= 500;
				break;
			case BISHOP:
				opponent_sum -= 330;
				your_sum += 8*std::min( 8 - oppo_pieces[ i ].position%8 , oppo_pieces[ i ].position%8);
				your_sum += 8*std::min( 8 - oppo_pieces[ i ].position/8 , oppo_pieces[ i ].position/8);
				break;
			case KNIGHT:
				opponent_sum -= 300;
				your_sum += 7*std::min( 8 - oppo_pieces[ i ].position%8 , oppo_pieces[ i ].position%8);
				your_sum += 7*std::min( 8 - oppo_pieces[ i ].position/8 , oppo_pieces[ i ].position/8);
				break;
			case PAWN:
				opponent_sum -= 100;
				if ( oppo_pieces[ i ].position%8 != 0 && b->B[ oppo_pieces[ i ].position - 9 ].piece != EMPTY ) {
					opponent_sum += 10;
				}
				if ( oppo_pieces[ i ].position%8 != 7 && b->B[ oppo_pieces[ i ].position - 7 ].piece != EMPTY ) {
					opponent_sum += 10;
				}
				break;
			default:
				continue;
		}
	}
	return your_sum + opponent_sum;
}