#include "chiass_v1.hh"

std::vector<Move> ParseOpening( FILE* f ) {
    std::vector<Move> returned;
    char beg[2];
    char end[2];
    Move added;
    while (!feof(f)) {
        fscanf(f, "%s %s\n", beg, end);
        added.beg = (square) ( (int) beg[0] -97 + ( (int) beg[1] - 49 ) * 8 );
        added.end = (square) ( (int) end[0] -97 + ( (int) end[1] - 49 ) * 8 );
        returned.push_back( added );
    }
    return returned;
}