#include "chiass_v1.hh"

// TODO : you can actually optimize the diagonals search
// instead of checking the condition in a while, you could just calculate how far you are from both borders
// then, take the min, and go in a for loop
/*
 * \return a value indicating if the piece is pinned down : \
 * \return 0 if it is pinned down \
 * \return 1 if it isn't pinned down \
 * \return 2 if it can move up and down \
 * \return 3 if it can move left and right \
 * \return 4 if it can move diagonaly up left/down right \
 * \return 5 if it can move diagonaly up right/down left \
 */
u8 IsPinnedDown( const Board* b , const PieceAndPosition p ) {
// checking if the king is in any line of view from the piece
	u8 line_of_view;
//Checking lines ===========================================================================
//	vertical ====================================================================
//		ascending =====================================
	line_of_view = 2;
	u8 checking_king = ( p.position + 8 );
	while ( checking_king < 64 ) {
		if ( b->B[ checking_king ].piece != EMPTY ) {
// WARNING : this needs be changed if we add values to pieces, or if the board isn't properly cleaned up
// TODO : unit testing is the wayyyy babyyyyy
			if ( b->B[ checking_king ].piece >= KING && b->B[ checking_king ].colour == p.colour ) {
				goto king_in_line_of_view;
			}
			break;
		}
		checking_king += (u8) 8;
	}
//		descending =====================================
	line_of_view ++;
	checking_king = ( p.position - 8 );
	while ( checking_king < 64 ) {
		if ( b->B[ checking_king ].piece != EMPTY ) {
// WARNING : this needs be changed if we add values to pieces, or if the board isn't properly cleaned up
// TODO : unit testing is the wayyyy babyyyyy
			if ( b->B[ checking_king ].piece >= KING && b->B[ checking_king ].colour == p.colour ) {
				goto king_in_line_of_view;
			}
			break;
		}
		checking_king -= (square) 8;
	}
//	horizontal ====================================================================
//		right ======================================================
	line_of_view ++;
	checking_king = ( p.position + 1 );
	while ( checking_king%8 != 0 ) {
		if ( b->B[ checking_king ].piece != EMPTY ) {
// WARNING : this needs be changed if we add values to pieces, or if the board isn't properly cleaned up
// TODO : unit testing is the wayyyy babyyyyy
			if ( b->B[ checking_king ].piece >= KING && b->B[ checking_king ].colour == p.colour ) {
				goto king_in_line_of_view;
			}
			break;
		}
		checking_king ++;
	}
//		left =======================================================
	line_of_view ++;
	checking_king = ( p.position - 1 );
	while ( checking_king != 7 ) {
		if ( b->B[ checking_king ].piece != EMPTY ) {
// WARNING : this needs be changed if we add values to pieces, or if the board isn't properly cleaned up
// TODO : unit testing is the wayyyy babyyyyy
			if ( b->B[ checking_king ].piece >= KING && b->B[ checking_king ].colour == p.colour ) {
				goto king_in_line_of_view;
			}
			break;
		}
		checking_king --;
	}




//Checking diagonals ====================================================================
//		top left ===========================================================
	line_of_view ++;
	checking_king = ( p.position + 7 );
	while ( checking_king < 64 && checking_king%8 != 7 ) {
		if ( b->B[ checking_king ].piece != EMPTY ) {
// WARNING : this needs be changed if we add values to pieces, or if the board isn't properly cleaned up
// TODO : unit testing is the wayyyy babyyyyy
			if ( b->B[ checking_king ].piece >= KING && b->B[ checking_king ].colour == p.colour ) {
				goto king_in_line_of_view;
			}
			break;
		}
		checking_king += (square) 7;
	}
//		top right ===========================================================
	line_of_view ++;
	checking_king = ( p.position + 9 );
	while ( checking_king < 64 && checking_king%8 != 0 ) {
		if ( b->B[ checking_king ].piece != EMPTY ) {
// WARNING : this needs be changed if we add values to pieces, or if the board isn't properly cleaned up
// TODO : unit testing is the wayyyy babyyyyy
			if ( b->B[ checking_king ].piece >= KING && b->B[ checking_king ].colour == p.colour ) {
				goto king_in_line_of_view;
			}
			break;
		}
		checking_king += (square) 9;
	}
//		bottom right ===========================================================
	line_of_view ++;
	checking_king = ( p.position - 7 );
	while ( checking_king < 64 && checking_king%8 != 0 ) {
		if ( b->B[ checking_king ].piece != EMPTY ) {
// WARNING : this needs be changed if we add values to pieces, or if the board isn't properly cleaned up
// TODO : unit testing is the wayyyy babyyyyy
			if ( b->B[ checking_king ].piece >= KING && b->B[ checking_king ].colour == p.colour ) {
				goto king_in_line_of_view;
			}
			break;
		}
		checking_king -= (square) 7;
	}
//		bottom left ===========================================================
	line_of_view ++;
	checking_king = ( p.position - 9 );
	while ( checking_king < 64 && checking_king%8 != 7 ) {
		if ( b->B[ checking_king ].piece != EMPTY ) {
// WARNING : this needs be changed if we add values to pieces, or if the board isn't properly cleaned up
// TODO : unit testing is the wayyyy babyyyyy
			if ( b->B[ checking_king ].piece >= KING && b->B[ checking_king ].colour == p.colour ) {
				goto king_in_line_of_view;
			}
			break;
		}
		checking_king -= (square) 9;
	}
	return 1;



	king_in_line_of_view:
	switch ( line_of_view ) {
		case 2:// the king is in line, up from the piece, we look down to see if we encounter a queen or rook
			checking_king = ( p.position - 8 );
			while ( checking_king < 64 ) {
				if ( b->B[ checking_king ].piece != EMPTY ) {
					if ( b->B[ checking_king ].colour != p.colour ) {
						if ( b->B[ checking_king ].piece == QUEEN || b->B[ checking_king ].piece == ROOK ) {
// The piece may be partially or completely pinned down, depending on its type
							if ( p.piece == QUEEN || p.piece == ROOK || p.piece == PAWN ) {// they can move in a straigth line, up
								return 2;
							}
							else { // the piece cannot move up, it is effectively pinned down
								return 0;
							}
						}
					}
					return 1;// No danger for the KING
				}
				checking_king -= (square) 8;
			}
			break;

		case 3:// the king is in line, down from the piece, we look up to see if we encounter a queen or rook
			checking_king = ( p.position + 8 );
			while ( checking_king < 64 ) {
				if ( b->B[ checking_king ].piece != EMPTY ) {
					if ( b->B[ checking_king ].colour != p.colour ) {
						if ( b->B[ checking_king ].piece == QUEEN || b->B[ checking_king ].piece == ROOK ) {
// The piece may be partially or completely pinned down, depending on its type
							if ( p.piece == QUEEN || p.piece == ROOK || p.piece == PAWN ) {// they can move in a straigth line, down
								return 2;
							}
							else { // the piece cannot move down, it is effectively pinned down
								return 0;
							}
						}
					}
					return 1;// No danger for the KING
				}
				checking_king += (square) 8;
			}
			break;

		case 4: // the king is in line, right from the piece, we look left to see if we encounter a queen or rook
			line_of_view ++;
			checking_king = ( p.position - 1 );
			while ( checking_king != 7 ) {
				if ( b->B[ checking_king ].piece != EMPTY ) {
					if ( b->B[ checking_king ].colour != p.colour ) {
						if ( b->B[ checking_king ].piece == QUEEN || b->B[ checking_king ].piece == ROOK ) {
// The piece may be partially or completely pinned down, depending on its type
							if ( p.piece == QUEEN || p.piece == ROOK ) {// they can move in a straigth line, right/left
								return 3;
							}
							else { // the piece cannot move right or left, it is effectively pinned down
								return 0;
							}
						}
					}
					return 1;// No danger for the KING
				}
				checking_king --;
			}
			break;

		case 5:// the king is in line, left from the piece, we look right to see if we encounter a queen or rook
			checking_king = ( p.position + 1 );
			while ( checking_king%8 != 0 ) {
				if ( b->B[ checking_king ].piece != EMPTY ) {
					if ( b->B[ checking_king ].colour != p.colour ) {
						if ( b->B[ checking_king ].piece == QUEEN || b->B[ checking_king ].piece == ROOK ) {
// The piece may be partially or completely pinned down, depending on its type
							if ( p.piece == QUEEN || p.piece == ROOK ) {// they can move in a straigth line, right/left
								return 3;
							}
							else { // the piece cannot move right or left, it is effectively pinned down
								return 0;
							}
						}
					}
					return 1;// No danger for the KING
				}
				checking_king ++;
			}
			break;

		case 6: // the king is in diagonal, up left from the piece, we look bottom right to see if we encounter a queen or bishop
			checking_king = ( p.position - 7 );
			while ( checking_king < 64 && checking_king%8 != 0 ) {
				if ( b->B[ checking_king ].piece != EMPTY ) {
					if ( b->B[ checking_king ].colour != p.colour ) {
						if ( b->B[ checking_king ].piece == QUEEN || b->B[ checking_king ].piece == BISHOP ) {
// The piece may be partially or completely pinned down, depending on its type
							if ( p.piece == QUEEN || p.piece == BISHOP || p.piece == PAWN ) {// they can move in diagonal, up left/down right
								return 4;
							}
							else { // the piece cannot move up left/down right, it is effectively pinned down
								return 0;
							}
						}
					}
					return 1;// No danger for the KING
				}
				checking_king -= (square) 7;
			}
			break;

		
		case 7: // the king is in diagonal, up right from the piece, we look bottom left to see if we encounter a queen or bishop
			checking_king = ( p.position - 9 );
			while ( checking_king < 64 && checking_king%8 != 7 ) {
				if ( b->B[ checking_king ].piece != EMPTY ) {
					if ( b->B[ checking_king ].colour != p.colour ) {
						if ( b->B[ checking_king ].piece == QUEEN || b->B[ checking_king ].piece == BISHOP ) {
// The piece may be partially or completely pinned down, depending on its type
							if ( p.piece == QUEEN || p.piece == BISHOP || p.piece == PAWN ) {// they can move in diagonal, up right/down left
								return 5;
							}
							else { // the piece cannot move up left/down right, it is effectively pinned down
								return 0;
							}
						}
					}
					return 1;// No danger for the KING
				}
				checking_king -= (square) 9;
			}
			break;

		case 8:// the king is in diagonal, bottom right from the piece, we look top left to see if we encounter a queen or bishop
			checking_king = ( p.position + 7 );
			while ( checking_king < 64 && checking_king%8 != 7 ) {
				if ( b->B[ checking_king ].piece != EMPTY ) {
					if ( b->B[ checking_king ].colour != p.colour ) {
						if ( b->B[ checking_king ].piece == QUEEN || b->B[ checking_king ].piece == BISHOP ) {
// The piece may be partially or completely pinned down, depending on its type
							if ( p.piece == QUEEN || p.piece == BISHOP || p.piece == PAWN ) {// they can move in diagonal, up left/down right
								return 4;
							}
							else { // the piece cannot move up left/down right, it is effectively pinned down
								return 0;
							}
						}
					}
					return 1;// No danger for the KING
				}
				checking_king += (square) 7;
			}
			break;

		case 9:// the king is in diagonal, bottom left from the piece, we look top right to see if we encounter a queen or bishop
			checking_king = ( p.position + 9 );
			while ( checking_king < 64 && checking_king%8 != 0 ) {
				if ( b->B[ checking_king ].piece != EMPTY ) {
					if ( b->B[ checking_king ].colour != p.colour ) {
						if ( b->B[ checking_king ].piece == QUEEN || b->B[ checking_king ].piece == BISHOP ) {
// The piece may be partially or completely pinned down, depending on its type
							if ( p.piece == QUEEN || p.piece == BISHOP || p.piece == PAWN ) {// they can move in diagonal, up right/down left
								return 5;
							}
							else { // the piece cannot move up left/down right, it is effectively pinned down
								return 0;
							}
						}
					}
					return 1;// No danger for the KING
				}
				checking_king += (square) 9;
			}
	}

	return 1;// No danger for the KING
}
