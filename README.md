![chIAss_logo](doc/chIAss_logo.png "chIAss's logo")
**chIAss** is a simple AI project for the game of chess.<br>
It is mainly written in C, with some C++ for convenience reasons.<br>
The project started on Tuesday 19/07/2022 and was active until its first checkpoint on Friday 29/07/2022.<br>
At this date, the AI was pretty much working, although really slow and not able to perform "complex" moves such as castling.<br>
No improvements will be made to this version of the AI, it will stay as a statement of performance and a stable weak version.<br>
A new project will be made, to create a better version.<br>
Modifications made to this repository after the 29/07 will be fixes (so that this version is actually stable), cleaning, reorganization or backward compatibility.

### Compiling
#### tests :
	make chiass_v1_test
	make test_mats
	make test_time
	(make test_inputs)

#### Playing chIAss_v1 :
	make test_chIAss


### Performance
After a standard opening, chIAss_v1 takes approximately 1 min to explore possibilities up to depth 5, then 25 min to reach depth 6.<br>
For a standard midgame position, chIAss_v1 takes approximately 25 min to explore possibilities up to depth 5.<br>
Results are reproducible with test_time.

### Why C?
Because fast.

### Why C++?
Because CUTE.

### AI good?
AI bad.